# variational-plasma

Welcome to the lecture [Variational principles for collective plasma motion](https://campus.tum.de/tumonline/pl/ui/$ctx;design=pl;header=max;lang=de/webnav.ini) in the summer semester of 2023 at the Department of Mathematics at TU Munich.

The course material (lecture notes in .tex, literature) will be constantly updated in this repo.

For questions, please contact [Stefan Possanner](stefan.possanner@ipp.mpg.de).

## Abstract

In a gas of charged particles (plasma), the long-range character of the electromagnetic force is responsible for new wave phenomena. Mathematical models for this "collective" behaviour of plasma are closely related to the Euler equations and the Boltzmann equation for neutral gases, but are richer in the sense that they have to account for the Lorentz force via self-consistent electromagnetic fields. Such models can be conveniently derived from Hamilton's principle for stationary action. We will learn how to apply the action principle in Lagrangian and Eulerian labelling. While the former allows for unconstrained variations of the unknowns, the latter features constrained variations derived from geometric conservation laws. The derivation of these constraints is known as Euler-Poincaré reduction and goes back to a work of Newcomb (1962). In order to understand this procedure we will briefly review notions like flow maps, differential forms and calculus of variations. In the end, we will be able to derive plasma models like MHD, Vlasov-Maxwell, drift-kinetics, and hybrid versions of these models, and study the conservation properties arising from Noether's theorem.

## Literature

- Hunter, Nachtergale
