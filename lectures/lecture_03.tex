\section{Lecture 3: \emph{``Flow Maps''}} \label{sec:flow}

\subsection{Some basics about ODEs}

 A classical\footnote{as opposed to a quantum mechanical} microscopic description of a dynamical system is given by equations for motion for its "atomic" constituents, be it particles, molecules or fluid elements. The atomic equations
 are of the form
 \be \label{ivp}
 \dot \qb(t) = G(\qb(t), t)\,,\qquad \qb(t_0) = \qb_0\,,
 \ee
 where $t\in\RR$, $\qb(t)\in \Omega$ with $\Omega \subset \RR^n$ open, $G:\Omega\times \RR \to \RR^n$ is called  the \emph{vector field} or \emph{direction field} of the ODE and $\qb_0 \in \Omega$ is an initial condition at time $t_0$. In case that $G = G(\qb(t))$ is time-independent the system \eqref{ivp} is called \emph{autonomous}. $G$ comprises all aspects of the microscopic dynamics, including possible mean field interactions; in plasma physics it will depend on the electromagnetic fields, which have to be determined by some other equations, for instance Maxwell's equations. 

\begin{definition}
    \textbf{(Maximal solution.)}
    By a solution of the IVP \eqref{ivp} we mean a continuously differentiable curve $\qb: I\to \Omega$ on some interval $I \subset \RR$ containing $t_0$ that satisfies both relations in \eqref{ivp}. A \emph{maximal solution} is such that the interval $I$ cannot be further extended. If $I = \RR$ we speak of a \emph{global solution}.
\end{definition}

The \emph{graph} of a solution $\qb(t)$ to \eqref{ivp} is defined as the set
$$
 \gamma_\qb = \{(t, \qb(t)) \in \RR \times \Omega \ : \ t \in I\}\,.
$$
By contrast, the \emph{orbit} of a solution $\qb(t)$ to \eqref{ivp} is defined as its image,
$$
 \omega_\qb = \{\qb(t) \in \Omega\ : \ t \in I\}\,.
$$ 
For simplicity, let us assume that $G$ is such that \eqref{ivp} is defined for all $t \in \RR$ (this does not mean that a solution exists for all $t \in \RR$, in general $I \subset \RR$). More precisely, we have

\begin{assumption} \label{lipschitz}
    \textbf{(Continuity of $G$.)}\\
    1) $G$ is \emph{continuous} on $\Omega \times \RR$.

    \noindent
    2) $G$ is \emph{locally Lipschitz continuous} on $\Omega \times \RR$ with respect to its first argument, i.e. for each $(\qb,t) \in \Omega \times \RR$ there exists a neighbourhood $U_\qb \in \Omega$ such that
    $$
    \frac{||G(\qb_1, t) - G(\qb_2, t)||}{||\qb_1 - \qb_2||} \leq \ell < \infty\qquad \qb_1,\qb_2 \in U_\qb\,.
    $$
\end{assumption}

Lipschitz continuity is weaker than differentiability. Any differentiable function has by definition bounded first derivative, and is thus locally Lipschitz. However, the converse is not true: the function $x\mapsto |x|$ is Lipschitz with $\ell=1$ but not differentiable (at $x=0$). The function $x\mapsto \sqrt x$ is not Lipschitz, as it becomes infinitely steep towards $x = 0$.

\begin{theorem} \label{lindelöf}
    \textbf{(Picard-Lindelöf, Cauchy-Lipschitz.)}
    Under the Assumption \ref{lipschitz}, the IVP \eqref{ivp} has a unique maximal solution for each $(t_0,\qb_0) \in \RR \times \Omega $. We denote by $I(t_0,\qb_0) \subset \RR$ the corresponding maximal interval.
\end{theorem}

Two different proofs of this Theorem can be found for instance in Theorem 4.17 and Theorem 4.22 of \cite{odes}, respectively.\\

\noindent
The \emph{flow map} $\Phi$ of the IVP \eqref{ivp} is introduced as the solution with explicit dependence on the initial condition $\qb(t_0) = \qb_0$. More precisely, $\Phi: \tn{Dom}\, \Phi \to \Omega$ with the domain 
$$
\tn{Dom}\, \Phi =\{(t;t_0,\qb_0) \in \RR \times\RR \times \Omega \ : \ t\in I(t_0, \qb_0)\}\,,
$$
and such that $t \mapsto \Phi(t;t_0,\qb_0)$ is the unique solution of \eqref{ivp}, 
$$
 \dot \Phi = G(\Phi, t)\qquad \Phi(t_0;t_0,\qb_0) = \qb_0\,.
$$
Hence, $\Phi(t_0;t_0,\cdot )$ is the identity on $\Omega$. We call the mapping $t \mapsto \Phi(t;t_0,\qb_0)$ the propagation of $\qb_0$ from $t_0$ to $t$, or short the propagation $t_0\to t$. 

\begin{theorem} \label{semigroup}
    \textbf{(Semigroup property.)}
    Suppose that $G$ in \eqref{ivp} satisfies Assumption \ref{lipschitz}. Let $t_1\in I(t_0,\qb_0)$ be an element of the maximal interval. The propagation from $t_0\to t$ can be split into $t_0 \to t_1$ and $t_1 \to t$, hence
    $$
    \Phi(t;t_0,\qb_0) = \Phi(t;t_1,\Phi(t_1;t_0,\qb_0))\,,
    $$
    and $I(t_0, \qb_0) = I(t_1,\Phi(t_1;t_0,\qb_0))$.
    \begin{proof}
        The left-hand side corresponds to a curve $\tilde\qb(t)$ that satisfies \eqref{ivp}; the right-hand side corresponds to a curve $ \qb(t)$ that satisfies
        \be \label{semi:proof}
        \dot \qb = G(\qb,t)\qquad \qb(t_1) = \Phi(t_1;t_0,\qb_0)\,.
        \ee
        Since $\tilde \qb(t_1) = \Phi(t_1;t_0,\qb_0)$, clearly $\tilde \qb(t)$ also satisfies \eqref{semi:proof}, and by uniqueness the solutions and their maximal intervals must be the same.
    \end{proof}
\end{theorem}

\begin{corollary*}
    \textbf{(Equivalence relation.)} Suppose that $G$ in \eqref{ivp} satisfies Assumption \ref{lipschitz}. The relation
    $$
    (t_0,\qb_0) \sim (t_1, \qb) \quad \tn{if} \ t_1 \in I(t_0,\qb_0) \ \tn{and} \ \qb = \Phi(t_1; t_0,\qb_0)
    $$
    is an equivalence relation on $\RR \times \Omega$ and the graphs of the IVP \eqref{ivp} are the equivalence classes.
    \begin{proof}
        Using the semigroup property, it is straightforward to show reflexivity $(t_0,\qb_0) \sim (t_0,\qb_0)$, symmetry $(t_1,\qb) \sim(t_0,\qb_0)$, and transitivity $(t_0,\qb_0) \sim (t_2,\qb_2)$ if $(t_1,\qb) \sim (t_2,\qb_2)$. Moreover, by definition the point $(t_1,\qb) \in I(t_0,\qb_0) \times \Omega$ lies on the graph of the solution of the IVP, which are thus the equivalence classes.
    \end{proof}
\end{corollary*}

An equivalence relation partitions a set into pairwise disjoint subsets (the equivalence
classes), whose union forms the whole set. Therefore, the above Corollary states that the
graphs of two solutions with initial conditions at $(t_0, \qb_0)$ and at $(t_1, \qb)$ are either identical or disjoint, i.e. different graphs do not intersect.\\


\subsection{Autonomous systems}

Let us now focus on autonomous systems, where the vector field $G$ is independent of time,
 \be \label{ivp:auto}
 \dot \qb(t) = G(\qb(t))\,,\qquad \qb(t_0) = \qb_0\,.
 \ee
Note that any IVP \eqref{ivp} can be brought into autonomous form by making time $t = t(\tau)$ a dependent variable ($\tau \in \RR$ denotes the independent variable),
$$
\left\{
\begin{aligned}
    \dot \qb(\tau) &= G(\qb(\tau), t(\tau))\,, \quad && \qb(\tau_0) = \qb_0\,,
    \\[1mm]
    \dot t(\tau) & = 1 && t(\tau_0) = \tau_0\,.
\end{aligned}
\right.
$$

\begin{theorem} \label{translation}
    \textbf{(Translation invariance.)} Suppose that $G$ in \eqref{ivp:auto} satisfies Assumption \ref{lipschitz}. Then, for any $t_0,t_1 \in \RR$ and $\qb_0 \in \Omega$ we have
    $$
    \Phi(t;t_0,\qb_0) = \Phi(t + t_1 - t_0; t_1,\qb_0)\qquad \forall \ t \in I(t_0,\qb_0)\,, 
    $$
    and the maximal interval can be shifted
    $$
    I(t_0,\qb_0) = I(t_1,\qb_0) - t_1 + t_0 := \{t - t_1 + t_0\in \RR \ : \ t \in I(t_0,\qb_0)\}\,.
    $$
    \begin{proof}
        Suppose the curve $\qb(t)$ satisfies
        $$
        \dot \qb(t) = G(\qb(t))\,,\qquad \qb(t_1) = \qb_0\,,
        $$
        and define $\tilde \qb(t) = \qb(t + t_1 - t_0)$. Clearly, $\tilde \qb(t)$ is defined on $I(t_1, \qb_0) - t_1 + t_0  $ but, moreover, satisfies the IVP\eqref{ivp:auto}.
    \end{proof}
\end{theorem}

By using the translation invariance, without loss of generality we can set $t_0 = 0$ and write any autonomous system as
 \be \label{ivp:0}
 \dot \qb(t) = G(\qb(t))\,,\qquad \qb(0) = \qb_0\,.
 \ee
Hence, for autonomous systems we can abbreviate the flow  and maximal interval,
$$
\Phi(t,\qb_0) := \Phi(t;0,\qb_0)\,,\qquad I(\qb_0) := I(0,\qb_0)\,.
$$
It is important to recall that by definition the flow itself satisfies the IVP:
$$
 \dot \Phi(t,\qb_0) = G(\Phi(t,\qb_0))\,,\qquad \Phi(0,\qb_0) = \qb_0\,.
$$
Since the initial condition $\qb_0$ is always time-independent, the partial time-derivative of the flow is equal to the total time derivative,
$$
 \dot \Phi(t,\qb_0) \equiv \dt{} \Phi(t,\qb_0) = \pder{}{t}  \Phi(t,\qb_0)\,.
$$


\begin{definition}
\textbf{(Dynamical system.)}
If $I(\qb_0) = \RR$ (global solution) for all $\qb_0 \in \Omega$ the IVP \eqref{ivp:0} is called a \emph{dynamical system}. 
\end{definition}

\begin{theorem}
    \textbf{(Group property.)} Suppose that $G$ in \eqref{ivp:0} satisfies Assumption \ref{lipschitz} and that the IVP is a dynamical system. The flow map $\Phi(t;\qb_0)$ satisfies
    \begin{align*}
        \Phi(0,\qb_0) &= \qb_0 \qquad \forall \ \qb_0 \in \Omega\,,
        \\[1mm]
        \Phi(t+t_1,\qb_0) &= \Phi(t,\Phi(t_1,\qb_0)) \qquad \forall \ t,t_1 \in \RR\,.
    \end{align*}
    \begin{proof}
        The first relation follows from the definition. For the second we combine the semigroup property and the translation invariance:
        \begin{align*}
            \Phi(t+t_1,\qb_0) &\stackrel{\phantom{\tn{Thm. }\ref{semigroup}}}{=} \Phi(t+t_1;0,\qb_0) 
            \\[1mm]
            & \stackrel{\tn{Thm. }\ref{semigroup}}{=} \Phi(t+t_1;t_1,\Phi(t_1;0,\qb_0))
            \\[1mm]
            & \stackrel{\tn{Thm. }\ref{translation}}{=} \Phi(t;0,\Phi(t_1;0,\qb_0))
            \\[1mm]
            & \stackrel{\phantom{\tn{Thm. }\ref{semigroup}}}{=} \Phi(t,\Phi(t_1,\qb_0))\,. 
        \end{align*}
    \end{proof}
\end{theorem}

In the case of dynamical systems, the equivalence relation from the Corollary to Theorem \ref{semigroup} becomes 
$$
\qb_0 \sim \qb \quad \tn{if} \ \exists \ t \in \RR\ \tn{such that} \ \qb = \Phi(t,\qb_0)\,.
$$
Hence the equivalence classes are the orbits of the IVP. This means that the flow $\Phi(t,\cdot): \Omega\to \Omega$ is an invertible map. If, moreover, this map is differentiable for all $t$, one calls $\{\Phi(t,\cdot)\}_{t\in\RR}$ a \emph{diffeomorphism group} on $\Omega$. We shall use the notation $\Phi_t(\cdot) \equiv \Phi(t,\cdot)$ and write $\Phi_t:\qb_0\to \qb$ for elements of this group. The Jacobian $D\Phi_t:\Omega\to \RR^{n\times n}$ ($n$ is the dimension of $\Omega$) and its determinant $J_t :\Omega\to \RR$ are defined by by
\be \label{def:J}
 D\Phi_{t,ij} = \pder{\Phi_{t,i}}{q_{0,j}}\,,\qquad J_t = \tn{det}\,D\Phi_t\,,
\ee
where $\Phi_{t,i}$ is the $i$-th component of the mapping and $\qb_0 = (q_{0,1},\ldots,q_{0,n}) \in \Omega$. The inverse of $\Phi_t:\qb_0\to \qb$ is denoted by $\Phi_t^{-1}:\qb\to \qb_0$. From 
$$
 q_{0,i} = \Phi^{-1}_{t,i}(\Phi_t(\qb_0))
$$
we obtain by differentiation
$$
 \delta_{ij} = \pder{q_{0,i}}{q_{0,j}} = \sum_k \pder{\Phi^{-1}_{t,i}}{q_k}(\Phi_t(\qb_0)) \pder{\Phi_{t,k}}{q_{0,j}}(\qb_0) \,,
$$
where $\delta_{ij}$ stands for the Kronecker delta, and hence
$$
 (D\Phi_t)^{-1} = D\Phi_t^{-1} \circ \Phi_t\,.
$$
The circle $\circ$ indicates the composition of mappings.

\begin{theorem} \label{thm:flow}
    \textbf{(Dynamics of the flow Jacobian.)} Let $\Phi_t:\qb_0\mapsto \qb$ be the diffeomorphism group associated to the IVP \eqref{ivp:0} with direction field $G$; moreover, denote by $\nabla G:\Omega\to \RR^{n\times n}$ with $\nabla G_{ij} = \pder{}{q_i} G_j$ the transpose Jacobian of $G$ and by $\nabla \cdot G = \sum_i \pder{}{q_i} G_i$ its divergence. Then the following three statements hold for the flow Jacobian defined in \eqref{def:J}:
    \begin{subequations}
    \begin{align}
     \dt{}D\Phi_t &= \nabla G^\top D\Phi_t  && D\Phi_0 = \mathds 1\,,\label{dt:jac}
     \\[1mm]
     \dt{}(D\Phi_t)^{-1} &= -(D\Phi_t)^{-1} \nabla G^\top   && (D\Phi_0)^{-1} = \mathds 1\,,  \label{dt:jacinv}
     \\[1mm]
     \dt{}|J_t| &= |J_t|\, \nabla \cdot G && J_0 = 1\,. \label{dt:det}
    \end{align}
    \end{subequations}
    \begin{proof}
        We prove \eqref{dt:jac} element-wise:
        \begin{align*}
            \dt{} D\Phi_{t,ij} & = \dt{} \pder{\Phi_{t,i}}{q_{0,j}}
            \\
            &= \pder{}{q_{0,j}} \dt{\Phi_{t,i}}
            \\
            &= \pder{}{q_{0,j}} G_i(\Phi_t(\qb_0))
            \\
            &= \sum_k \pder{G_i}{q_k}\pder{\Phi_{t,k}}{q_{0,j}}
            \\
            &= (\nabla G^\top D\Phi_t)_{ij}\,.
        \end{align*}
        The initial conditions are obvious. The statement \eqref{dt:jacinv} follows from
        \begin{align*}
            0 &= \dt{} \big[(D\Phi_t)^{-1} D\Phi_t \big]
        \\
        &= \left[\dt{} (D\Phi_t)^{-1} \right] D\Phi_t + (D\Phi_t)^{-1} \dt{} D\Phi_t 
        \\
        &= \left[\dt{} (D\Phi_t)^{-1} \right] D\Phi_t + (D\Phi_t)^{-1} \nabla G^\top D\Phi_t \,,
        \end{align*}
        where we inserted \eqref{dt:jac} in the last line. The result follows since $D\Phi_t$ is invertible. The initial conditions are again obvious. To prove the last statement, we use that the flow is differentiable in $t$ and Taylor expand the Jacobian as
        \begin{align*}
        D\Phi_{t+\eps} &= D\Phi_t + \eps \dt{}  D\Phi_t + O(\eps^2)
        \\
        &= D\Phi_t + \eps\, \nabla G^\top D\Phi_t + O(\eps^2)
        \end{align*}
        Moreover, since $D\Phi_t$ is invertible we can apply Sylvester's determinant theorem \cite{sylvester} to factor out $\tn{det}\,D\Phi_t$ in
        \begin{align*}
            \tn{det}D\Phi_{t+\eps} - \tn{det}\,D\Phi_t &= \tn{det}\left[ D\Phi_t + \eps\, \nabla G^\top D\Phi_t + O(\eps^2)\right] - \tn{det}\,D\Phi_t
            \\[1mm]
            &= \tn{det}\,D\Phi_t\,\tn{det}\left[ \mathds 1 + \eps\, \nabla G^\top + O(\eps^2)\right] - \tn{det}\,D\Phi_t
            \\[1mm]
            &= \tn{det}\,D\Phi_t\,\left[ 1 + \eps\, \nabla \cdot G + O(\eps^2)\right] - \tn{det}\,D\Phi_t
            \\[1mm]
            &= \eps\,\tn{det}\,D\Phi_t\, \nabla \cdot G + O(\eps^2)\,.
        \end{align*} 
        Here, in the second to last line we used the algebraic property $\tn{det}\,(\mathds  1 + \eps\, A) = 1 + \eps\,\tn{tr}\, A + O(\eps^2)$ where $\tn{tr}\, A$ denotes the trace of the matrix $A$. Dividing the last line by $\eps$ and taking the limit $\eps\to 0$ yields
        $$
        \dt{}J_t = J_t\, \nabla \cdot G\,.
        $$
         The modulus can be added because $J_t \neq 0$ for all $t$, which means that it is either always positive or always negative. The initial conditions are again obvious.
    \end{proof}
\end{theorem}
