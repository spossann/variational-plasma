\section{Lecture 7: \emph{``Euler-Poincaré reduction''}}

\subsection{Generic Lagrangian dynamics}
Let us start by recalling Hamilton's principle of stationary action from section \ref{sec:hamprinc}, 
\be \label{hamprinc:2}
\delta S = \delta \int_0^T L(\qb(t), \dot\qb(t))\,\tn d t = 0\,,
\ee
where $\qb:[0, T] \to \Omega$ with $\Omega \subseteq \RR^n$ an $m$-dimensional submanifold of $\RR^n$. The Lagrangian $L: T\Omega \to \RR,\, (\qb,\dot\qb) \mapsto L(\qb,\dot\qb)$, here assumed without explicit dependence on time, can be viewed as a mapping from the tangent bundle $T\Omega$ into the real numbers. Here, we used the abuse of notation discussed around equation \eqref{abuse}, identifying $(\qb,\dot\qb)$ with an element in $T\Omega$. For most dynamical systems, the Lagrangian is given as the difference between kinetic energy $K$ and potential energy $V$,
\be \label{L:usual}
 L = T - V\,.
\ee
The variational principle \eqref{hamprinc:2} leads to the system dynamics in the form of the Euler Lagrange equations \eqref{eulag1}:
\be \label{eulag2}
\left(\pder{L}{\qb} - \dt{}\pder{L}{\dot \qb} \right)_{\qb=\qb(t),\dot\qb = \dot \qb(t)} = 0\,,
\ee
which must be supplemented with suitable initial conditions to yield a unique solution. Remark that the ODE \eqref{eulag2} need not be first order, for instance in the case where $L$ is quadratic in $\dot \qb$. However, it can always be cast in the form of an autonomous, first-order ODE by introducing additional variables and thus increasing the dimension of $\Omega$.

\begin{example} \label{ex:Vlag}
 Consider the configuration space $\qb =\xb\in \RR^n$ with the Lagrangian
 $$
 L(\qb,\dot \qb) = \frac{m|\dot \xb|^2}{2} - V(\xb)\,.
 $$
 The Euler-Lagrange equations yield
 $$
 m \ddot \xb = -\nabla V(\xb)\,,
 $$
 which is Newton' law with the force $F = -\nabla V$. This is a second-order ODE which needs two initial conditions, $\xb(0) = \xb_0$ and $\dt\xb(0) = \vb_0$. We can write it as a autonomous, first-order ODE by introducing the variable $\vb = \dot \xb$:
 $$
 \left\{
 \begin{aligned}
  \dot \xb &= \vb \qquad &&\xb(0) = \xb_0\,,
  \\[1mm]
  \dot \vb &= -\nabla V(\xb) \qquad && \vb(0) = \vb_0\,.
 \end{aligned}
   \right.
$$
Indeed, denoting $\qb=(\xb,\vb)\in \RR^{2n}$, these equations can be obtained directly from the \emph{phase space Lagrangian}
\be \label{L:phasespace}
 L(\qb,\dot \qb) = \vb \cdot \dot \xb - H(\xb,\vb)\,,
\ee
where $H:\RR^n \to \RR$ denotes the \emph{Hamiltonian} of the system,
$$
 H(\xb,\vb) = \frac{m|\vb|^2}{2} + V(\xb)\,,
$$
which corresponds to the system's total energy. 
\end{example}

Lagrangians of the form \eqref{L:phasespace} are generic,
\be \label{L:generic}
 L(\qb,\dot\qb) = \gamma(\qb) \cdot \dot \qb - H(\qb)\,,
\ee
defined on the one hand by the \emph{Hamiltonian} $H:\Omega \to \RR$, and on the other hand by the \emph{symplectic form} $\gamma:\Omega \to \RR^n$
they can be obtained from configuration space Lagrangians ($\Omega$ only describes the particle positions) via a \emph{Legendre transform} (see standard mechanics textbooks). The corresponding Euler-Lagrange equations read
$$
 \omega(\qb) \cdot \dot \qb = \nabla H(\qb)\,,
$$
where $\omega:\Omega \to \RR^{n\times n},\, \omega = D\gamma^\top - D\gamma$ is a skew symmetric matrix called the \emph{Lagrange matrix}. In case that $\omega$ is invertible for all $\qb$, one obtains a Hamiltonian system,
$$
 \dot \qb = \omega^{-1}(\qb) \cdot \nabla H(\qb)\,,
$$
which by construction conserves energy:
$$
 \dt{}H(\qb) = \nabla H \cdot \dot \qb = \nabla H \cdot \omega^{-1} \cdot \nabla H = 0\,,
$$
due to the skew-symmetry of $\omega^{-1}$.


\subsection{Eulerian velocity and right-invariance}
In what follows we consider dynamical systems with a flow map $\Phi_t:\Omega \to\Omega$ (a diffeomorphism) satisfying
$$
\dot \Phi_t(\qb_0) = G(\Phi_t(\qb_0))\,,\qquad \Phi_0(\qb_0) = \qb_0\,.
$$
Clearly, $G(\Phi_t(\qb_0))$ can be viewed as the velocity\footnote{In case of a Hamiltonian system, $G=\omega^{-1}\cdot \nabla H$.} of the "particle" $\qb_0$ at time $t$. This is the \emph{Lagrangian} view of the dynamics. However, one could ask the following:\\

"What is the velocity $\ub(t,\qb)$ as a function of time at a fixed point $\qb \in \Omega$?"\\

\noindent
A straightforward definition is:
\be \label{def:u}
 \ub(t,\qb) := \dot \Phi_t \circ \Phi_t^{-1} (\qb) \equiv \dot \Phi_t(\Phi_t^{-1} (\qb)) \,.
\ee
This can be understood as follows: for fixed $\qb\in\Omega$, at each time $t$ we search the "particle" $\qb_0\in\Omega$ that arrives at $\qb$ through the Lagrangian dynamics, such that $\qb = \Phi_t(\qb_0)$. This particle is thus given by $\qb_0 = \Phi_t^{-1}(\qb)$. Then we just calculate the velocity of this particle at time $t$, which amounts to the above definition of $
\ub(t,\qb)$.

Our aim now is to derive a PDE for the \emph{Eulerian} velocity $\ub(t,\qb)$ from the original Hamilton's principle \eqref{hamprinc:2}. For this we will assume the Lagrangian $L$ to be an integral over the Lagrangian coordinates $\qb_0$ and express the action $S$ in terms of the flow map:
\be
 S(\Phi_t, \dot \Phi_t) = \int_0^T \int_{\Omega} \ell_0(\Phi_t(\qb_0), \dot\Phi_t(\qb_0), \qb_0)\,\tn d \qb_0\,\tn dt\,.
\ee
Here, $\ell_0:T\Omega \times \Omega \to \RR$ is a \emph{Lagrange density}, with explicit dependence on $\qb_0$. We will study a particular class of Lagrange densities, namely those which are \emph{right-invariant}:

\begin{definition}
 A Lagrange density $\ell_0:T\Omega \times \Omega \to \RR$ is called {\bf right-invariant} if, under the change of coordinates $\qb_0 = \Phi_t^{-1}(\qb)$, $\Omega \to\Omega$, it satisfies
 \be \label{rightinv}
    \int_\Omega \ell_0(\Phi_t(\qb_0), \dot\Phi_t(\qb_0), \qb_0) \,\tn d\qb_0 = \int_{\Omega} \ell(t,\qb, \ub(t,\qb))\,\tn d \qb\,,
 \ee
 where $\ell$ does not depend on $\Phi_t$ (or its inverse).
\end{definition}

\begin{example} \label{ex:rightinv}
 Let $L(\qb,\dot \qb)$ stand for the Lagrangian of some single-particle dynamics in $\Omega \subseteq \RR^n$ open, and suppose $\hat f^n:\RR \times \Omega\to\RR$ is the component of an $n$-form that is transported with the flow map $\Phi_t$ associated to $L$, according to Theorem \ref{transp:vol}. Then the Lagrange density 
 $$
 \ell_0(\Phi_t, \dot\Phi_t,\qb_0) = \hat f^n(0,\qb_0)\, L(\Phi_t, \dot \Phi_t)
 $$
 is right-invariant, since under the change of coordinates $\qb_0 = \Phi_t^{-1}(\qb)$ we have
 $$
 \begin{aligned}
 \int_\Omega \ell_0(\Phi_t, \dot\Phi_t,\qb_0)\,\tn d\qb_0 &= \int_\Omega\hat f^n(0,\qb_0)\, L(\Phi_t, \dot \Phi_t)\,\tn d\qb_0 
 \\
 &=\int_{\Omega} \frac{\hat f^n(0) \circ \Phi_t^{-1}}{|\tn{det} D\Phi_t \circ \Phi_t^{-1}|}\,L(\Phi_t \circ \Phi_t^{-1}, \dot\Phi_t \circ \Phi_t^{-1})\,\tn d \qb 
 \\
 &= \int_{\Omega} \hat f^n(t,\qb)\,L(\qb, \ub(t,\qb))\,\tn d \qb 
 \\
 &= \int_{\Omega} \ell(t,\qb,\ub(t,\qb))\,\tn d \qb \,.
 \end{aligned}
 $$ 
 Here, in the second line we inserted the measure coming form the change of coordinates,
 $$
 \tn d\qb = |\tn{det} D\Phi_t(\qb_0)| \tn d \qb_0\,,
 $$
 and to get to the third line we used the second statement of Theorem \ref{transp:vol}, namely
 $$
 \hat f^n(t, \Phi_t(\qb_0)) = \frac{\hat f^n(0,\qb_0)}{|\tn{det}D\Phi_t(\qb_0)|}\,.
 $$
\end{example}

\subsection{Euler-Poincaré reduction theorem}

We are now equipped to state the main theorem of this lecture, which allows to relate Lagrangian and Eulerian dynamics.

\begin{theorem} \label{thm:EP}
 {\bf (Euler-Poincaré reduction.)} Let $\Omega \subseteq\RR^n$ denote an $m$-dimensional submanifold of $\RR^n$. Suppose $\ell_0:T\Omega\times\Omega\to \RR$ is a right-invariant Lagrange density. Moreover, let $\Phi_t:\Omega\to\Omega$ stand for a diffeomorphism for each $t\in \RR$. Then, the following statements are equivalent:
 \begin{enumerate}
  \item The Lagrangian dynamics are given by Hamilton's principle,
  $$
  \delta \int_0^T \int_{\Omega} \ell_0(\Phi_t(\qb_0), \dot\Phi_t(\qb_0), \qb_0)\,\tn d \qb_0\,\tn dt = 0\,.
  $$
  \item The flow $\Phi_t$ satisfies the Euler-Lagrange equations
  $$
  \pder{\ell_0}{\Phi_t} - \dt{} \pder{\ell_0}{\dot\Phi_t} = 0\,.
  $$
  \item The variational principle 
  \be \label{varprinc:Su}
  \delta S(\ub) = \delta \int_0^T \int_{\Omega} \ell(\qb, \ub(t,\qb))\,\tn d \qb\,\tn dt = 0
  \ee
  holds for $S$ as a functional of $\ub$, where $\ub$ has the constrained variations
  \be \label{constrainedvar}
  \delta \ub = \dot \etab + [\ub, \etab] = \dot \etab + \ub \cdot \nabla \etab - \etab \cdot \nabla \ub\,,
  \ee
  for arbitrary $\etab: \RR\times \Omega \to \Omega $.
  \item The \emph{Euler-Poincaré equations} hold:
  \be \label{eulpoinc}
   \dt{} \fder{\ell}{\ub} + \nabla \cdot \left( \ub \otimes \fder{\ell}{\ub}\right) + \nabla \ub \cdot \fder{\ell}{\ub} = 0\,.
  \ee
 \end{enumerate}
 \begin{proof}
  $1.) \leftrightarrow 2.)$ is immediate by taking the unconstrained variations $\delta\ell_0 / \delta\Phi_t = 0$.\\
  
  $1.) \leftrightarrow 3.)$ The first step is immediate by the right invariance of $\ell_0$:
  $$
  0 = \delta \int_0^T \int_\Omega \ell_0(\Phi_t(\qb_0), \dot\Phi_t(\qb_0), \qb_0) \,\tn d\qb_0 = \delta \int_0^T\int_{\Omega} \ell(t,\qb, \ub(t,\qb))\,\tn d \qb = \delta S(\ub)\,.
  $$
  It remains to compute the variations $\delta \ub$; they follow from the definition 
  $$
  \ub(t,\qb) := \dot \Phi_t \circ \Phi_t^{-1}(\qb)\,.
  $$
  Indeed, recalling from section \ref{sec:hamsprinc} the definition of the variation of a path,
  \be \label{recall:delphi}
  \delta \Phi_t(\qb_0) := \dvar \tilde \Phi_t(\qb_0,\eps)\,,
  \ee
  where $\tilde \Phi_t(\qb_0,\eps)$ is such that $\tilde \Phi_t(\qb_0,0) = \Phi_t(\qb_0)$, it follows that 
  $$
  \delta \ub(t,\qb) = \dvar \left[\dot{\tilde{\Phi}}_t(\qb_0,\eps) \circ \tilde \Phi_t^{-1}(\qb, \eps) \right]
  $$
  Before we proceed, let us clarify some notation; for $\ab,\bb: \Omega \to \Omega$ differentiable functions, we write
  $$
  \begin{aligned}
   \ab \circ \bb = \ab(\bb)\,,\qquad \nabla \ab = D\ab^\top = \left(\pder{a_j}{q_i}\right)_{i,j}\,,\qquad \pder{}{q_i} (\ab \circ \bb) = \pder{\bb}{q_i} \cdot \nabla \ab \circ \bb\,.
  \end{aligned}
  $$
  The first is just the composition of functions, the second relates the gradient of the function to its Jacobian matrix, and the third is the derivative of the composition via the chain rule. We shall use the dot to indicate the contraction of a vector with a matrix, either from the left or from the right; we do not use a dot for matrix-matrix products. Hence, we obtain
  \be \label{delu:1}
  \begin{aligned}
  \delta \ub &= \left(\deps \dot{\tilde{\Phi}}_t \right) \circ \tilde \Phi_t^{-1} \bigg|_{\eps=0} + \deps \tilde \Phi_t^{-1} \cdot \nabla \widetilde{\dot \Phi_t} \circ \tilde \Phi_t^{-1} \bigg|_{\eps=0}\,.
  \end{aligned}
  \ee
  In the first term, we can exchange the time derivative with the $\eps$-derivative:
  $$
  \left(\deps \dot{\tilde{\Phi}}_t \right) \circ \tilde \Phi_t^{-1} \bigg|_{\eps=0} = 
  \left(\dt{} \deps \tilde{\Phi}_t \right) \circ \tilde \Phi_t^{-1} \bigg|_{\eps=0} 
  = \left(\dt{}\delta\Phi_t \right) \circ \Phi_t^{-1} \,.
  $$
  This suggests the definition of the function
  $$
  \etab(t,\qb) := \delta\Phi_t \circ \Phi_t^{-1}(\qb)\,.
  $$
  Since $\delta\Phi_t$ is an arbitrary variation of a curve (vanishing at the end points), $\etab$ is an arbitrary function on $\RR\times\Omega$.
  The time derivative and gradient of this function are, respectively,
  $$
  \begin{aligned}
  \dot \etab &= \left(\dt{}\delta\Phi_t \right) \circ \Phi_t^{-1} + \dot \Phi_t^{-1} \cdot \nabla \delta\Phi_t \circ \Phi_t^{-1}\,,
  \\[1mm]
  \nabla \etab &= \nabla \Phi_t^{-1} \nabla \delta\Phi_t \circ \Phi_t^{-1}\,.
  \end{aligned}
  $$
  From the last line we obtain $\nabla \delta\Phi_t \circ \Phi_t^{-1} = \nabla \Phi_t \nabla \etab$.
  Moreover, we can evaluate the second term in \eqref{delu:1} at $\eps=0$ to obtain 
  $$
  \delta \ub = \dot \etab - \dot \Phi_t^{-1} \cdot \nabla \Phi_t \nabla \etab + \delta\Phi_t^{-1} \cdot \nabla \dot \Phi_t \circ \Phi_t^{-1}\,.
  $$
  The last term can be written in terms of $\ub$ by using
  $$
  \nabla \ub = \nabla (\dot \Phi_t \circ \Phi_t^{-1}) = \nabla \Phi_t^{-1} \nabla \dot \Phi_t \circ \Phi_t^{-1}\,,
  $$
  which gives $\nabla \dot \Phi_t \circ \Phi_t^{-1} = \nabla \Phi_t \nabla \ub$ and therefore
  $$
  \delta \ub = \dot \etab - \dot \Phi_t^{-1} \cdot \nabla \Phi_t \nabla \etab + \delta\Phi_t^{-1} \cdot \nabla \Phi_t \nabla \ub\,.
  $$
  It remains to compute the time derivative and the variations of $\Phi_t^{-1}$; they follow immediately from
  $$
  \begin{aligned}
  0 &= \dt{}(\Phi_t \circ \Phi_t^{-1}) = \dot \Phi_t \circ \Phi_t^{-1} +  \dot \Phi_t^{-1} \cdot \nabla \Phi_t \qquad \implies\quad && \dot \Phi_t^{-1} \cdot \nabla \Phi_t = - \ub\,,
  \\[1mm]
  0 &= \delta(\Phi_t \circ \Phi_t^{-1}) = \delta \Phi_t \circ \Phi_t^{-1} + \delta \Phi_t^{-1} \cdot \nabla \Phi_t \qquad \implies\quad && \delta \Phi_t^{-1} \cdot \nabla \Phi_t = - \etab\,.
  \end{aligned}
  $$
  The variations of $\ub$ thus read
  $$
  \delta \ub = \dot \etab + \ub \cdot \nabla \etab - \etab \cdot \nabla \ub\,,
  $$
  where $\etab(t,\qb)$ is arbitrary.\\
  
  $3) \to 4)$: The variational principle \eqref{varprinc:Su} leads to
  $$
  \delta S(\ub) = \int_0^T \int_{\Omega} \fder{\ell}{\ub} \cdot \delta \ub\,\tn d \qb\,\tn dt = 0\,.
  $$
  Substituting the variations \eqref{constrainedvar} and integrating by parts (variations vanish at the boundaries) yields
  $$
  \begin{aligned}
   \delta S(\ub) &= \int_0^T \int_{\Omega} \fder{\ell}{\ub} \cdot \left( \dot \etab + \ub \cdot \nabla \etab - \etab \cdot \nabla \ub \right) \tn d \qb\,\tn dt
   \\[1mm]
   &= \int_0^T \int_{\Omega} \left[ - \dt{} \fder{\ell}{\ub} - \nabla \cdot \left( \ub \otimes \fder{\ell}{\ub} \right) - \nabla \ub \cdot \fder{\ell}{\ub} \right] \cdot \etab\, \tn d \qb\,\tn dt
   \\[2mm]
   &=0\,.
  \end{aligned}
   $$
   Since $\etab$ is arbitrary the Euler-Poincaré equations follow.
 \end{proof}
\end{theorem}


\subsection{Variations of differential forms}
The Euler-Poincaré reduction theorem from the previous sections shows us how to take variations of an action $S(\ub)$ with respect to the Eulerian velocity $\ub$. However, it may be the case that the right-invariant Lagrange density $\ell_0$ in $S$ depends also on differential forms that are transported with the flow $\Phi_t$. We saw one such case in Example \ref{ex:rightinv}. Because they are transported with the flow, their variations are constrained and have to be computed with care. We do this in two steps: first, we derive the variations of the flow Jacobian $D\Phi_t$. Second, we use the transport theorems of section \ref{sec:transp} to derive the variations of the transported differential forms.

\begin{theorem} 
 {\bf (Variations of the flow Jacobian.)} Let $\Phi_t:\Omega \to \Omega$ denote a diffeomorphism on the manifold $\Omega$ and define $\etab := \delta \Phi_t \circ \Phi_t^{-1}$, where $\delta \Phi_t$ denotes the variation defined in \eqref{recall:delphi}. Then we have the following variations of the flow Jacobian:
 \begin{subequations}
 \begin{align}
  \delta D\Phi_t &= D\delta\Phi_t\,, \label{del:1}
  \\[2mm]
  \delta D\Phi_t^{-1} &= - D\Phi_t^{-1} D\delta\Phi_t D\Phi_t^{-1}\,, \label{del:2}
  \\[2mm]
  \delta(\tn{det} D\Phi_t) &= \tn{det} D\Phi_t\, \nabla \cdot \etab\,.  \label{del:3}
 \end{align}
 \end{subequations}
 \begin{proof}
  Equation \eqref{del:1} is immediate because the $\eps$-derivative can be exchanged with the spatial derivatives. Equation \eqref{del:2} is also straightforward:
  $$
  0 = \delta(D\Phi_t D\Phi_t^{-1}) = D \delta \Phi_t D\Phi_t^{-1} + D\Phi_t \delta D\Phi_t^{-1} \ \ \implies\ \  \delta D\Phi_t^{-1} = - D\Phi_t^{-1} D\delta \Phi_t D\Phi_t^{-1}
  $$
  For equation \eqref{del:3} we have to work a little bit harder; first we state
  \begin{align*}
   \delta(\tn{det} D\Phi_t) &= \dvar \tn{det} D\tilde\Phi_t(\qb_0,\eps)
   \\[1mm]
   &= \lim_{\eps\to0} \frac{\tn{det} D\tilde\Phi_t(\qb_0,\eps) - \tn{det} D \Phi_t(\qb_0)}{\eps}\,.
  \end{align*}
   We can expand the first term in the numerator by using use Sylvester's determinant theorem \cite{sylvester}:
   \begin{align*}
    \tn{det} D\tilde\Phi_t &= \tn{det}[ D\Phi_t + \eps\,D\delta\Phi_t + O(\eps^2) ]
    \\[1mm]
    &= \tn{det} D\Phi_t\, \tn{det} \left( \mathds 1 + \eps\,D\delta\Phi_t D\Phi_t^{-1} \right) + O(\eps^2)
    \\[1mm]
    &=\tn{det} D\Phi_t \left( 1 + \eps\,\tn{tr} D\delta\Phi_t D\Phi_t^{-1} \right) + O(\eps^2)\,.
   \end{align*}
   Noting that the trace computes as
   $$
   \tn{tr} D\delta\Phi_t D\Phi_t^{-1} = \tn{tr} \nabla\Phi_t^{-1} \nabla\delta\Phi_t = \tn{tr}\nabla(\delta \Phi_t \circ \Phi_t^{-1}) = \nabla \cdot \etab\,,
   $$
   we have proved \eqref{del:3}.
 \end{proof}
\end{theorem}
We can now easily derive the variations of differential $p$-forms that are transported with the flow. We shall do this again for $p=0$, $p=1$ and $p=n$ for arbitrary manifold dimension $n$, and for $p=2$ only for the case $n=3$.

\begin{theorem} \label{thm:vardel}
 {\bf (Variations of transported $p$-forms.)} Let $\Phi_t:\Omega \to \Omega$ denote a diffeomorphism on the manifold $\Omega$ and define $\etab := \delta \Phi_t \circ \Phi_t^{-1}$, where $\delta \Phi_t$ denotes the variation defined in \eqref{recall:delphi}. Moreover, suppose $\hat f^0$, $\hat \Eb^1$ and $\hat f^n$ to be the components of a 0-, 1- and $n$-form on $\Omega$, respectively, and further suppose $\hat \Eb^2$ to be the components of a 2-form in the case $n=3$, all transported with the flow $\Phi_t$ according to the Theorems of section \ref{sec:transp}. The variations of these component functions are:
 \begin{subequations}
  \begin{align}
   \delta \hat f^0 &= -\etab \cdot \nabla \hat f^0\,, \label{variat:0}
   \\[1mm]
   \delta \hat \Eb^1 &= -\etab \cdot \nabla \hat \Eb^1 - \nabla \etab \cdot \hat \Eb^1\,, \label{variat:1}
   \\[1mm]
   \delta \hat \Eb^2 &= -\etab \cdot \nabla \hat \Eb^2 + \hat \Eb^2 \cdot \nabla \etab - \hat \Eb^2\,\nabla \cdot \etab \,, \qquad (n=3)\,, \label{variat:2}
   \\[1mm]
   \delta \hat f^n &= - \nabla \cdot (\etab \hat f^n)\,. \label{variat:n}
  \end{align}
 \end{subequations}
 \begin{proof}
  We will use in all cases the second point of the theorems from section \ref{sec:transp} as a starting point. For 0-forms, we have
  $$
  \hat f^0(t) \circ \Phi_t = \hat f^0(0)\,. 
  $$
  Taking variations on both sides leads to
  \begin{align*}
   &\delta(\hat f^0(t) \circ \Phi_t) = 0
   \\[1mm]
   \Leftrightarrow\quad & \delta\hat f^0(t) \circ \Phi_t + \delta\Phi_t \cdot \nabla \hat f^0(t) \circ \Phi_t = 0
   \\[1mm]
   \Leftrightarrow\quad & \delta\hat f^0(t)  + \delta\Phi_t \circ \Phi_t^{-1} \cdot \nabla \hat f^0(t) = 0\,,
  \end{align*}
  which proves \eqref{variat:0}.
  
  For $n$-forms we have
  $$
  \hat f^n(t) \circ \Phi_t \,\tn{det} D\Phi_t = \hat f^n(0)\,. 
  $$
  Taking variations on both sides and using equation \eqref{del:3} leads to
  \begin{align*}
   &\delta(\hat f^n(t) \circ \Phi_t\,\tn{det} D\Phi_t) = 0
   \\[1mm]
   \Leftrightarrow\quad & \left(\delta\hat f^n(t) \circ \Phi_t + \delta\Phi_t \cdot \nabla \hat f^0(t) \circ \Phi_t \right) \tn{det} D\Phi_t + \hat f^n(t)\circ \Phi_t\, \tn{det} D\Phi_t\,\nabla \cdot \etab= 0
   \\[1mm]
   \Leftrightarrow\quad & \delta\hat f^n(t)  + \delta\Phi_t \circ \Phi_t^{-1} \cdot \nabla \hat f^n(t) + \hat f^n \,\nabla \cdot \etab = 0\,,
  \end{align*}
  which proves \eqref{variat:n}.
  
  The proofs of \eqref{variat:1} and \eqref{variat:2} go accordingly, exactly as in the proofs of point 2.) of the theorems in section \ref{sec:transp}.
 \end{proof}
\end{theorem}

\begin{corollary} \label{cor:divB}
 In case that $\nabla \cdot \hat \Eb^2 = 0$ in three dimensions, we have
 \be \label{variat:2div}
 \begin{aligned}
 \delta \hat \Eb^2 &= -\etab \cdot \nabla \hat \Eb^2 + \hat \Eb^2 \cdot \nabla \etab - \hat \Eb^2 \,\nabla \cdot \etab + \etab\,\nabla \cdot \hat \Eb^2 
 \\[1mm]
 &= \nabla \times (\etab \times \hat \Eb^2)\,.
 \end{aligned}
 \ee
\end{corollary}


Finally, let us state a simple example of Euler-Poincaré reduction. For this we will combine the simple Lagrangian from Example \ref{ex:Vlag} with the right-invariant Lagrange density from Example \ref{ex:rightinv}.

\begin{example}
 Suppose a Lagrange density $\ell_0$ of the form
 $$
 \ell_0 = \hat \rho^n(0,\xb_0) \left( \frac{|\dot \Phi_t(\xb_0)|^2}{2} - V(\Phi_t(\xb_0))\right)\,,
 $$
 where $\hat \rho^n(0): \RR^n \to \RR$ is the component function of an $n$-form, transported with the flow $\Phi_t:\RR^n \to \RR^n$. From Example \ref{ex:rightinv} we already know that $\ell_0$ is right-invariant. Moreover, from Example \ref{ex:Vlag} we know that the corresponding Euler-Lagrange equations read
 $$
 m\ddot \Phi_t = - \nabla V(\Phi_t)\,,
 $$
 which are Newton's equations of motion in a central force field $-\nabla V$. We can now wonder what kind of fluid equations arise from this Lagrange density; the answer is obtained by applying the Euler-Poincaré reduction procedure. First, using the right-invariance of $\ell_0$, we obtain the action
 $$
 S(\ub, \hat \rho^n) =  \int_0^T \int_{\RR^n} \hat \rho^n(t,\xb) \left( \frac{|\ub(t,\xb)|^2}{2} - V(\xb)\right) \,\tn d\xb\,\tn d t = \int_0^T \int_{\RR^n} \ell(\ub,\hat \rho^n,\xb) \,\tn d\xb\,\tn d t\,.
 $$
 Taking variations yields
 \begin{align*}
 \delta S &= \int_0^T \int_{\RR^n} \left( \fder{\ell}{\ub} \cdot \delta \ub + \fder{\ell}{\hat \rho^n} \delta \hat \rho^n \right)\tn d\xb\,\tn d t
 \\[1mm]
 &= \int_0^T \int_{\RR^n} \left( \fder{\ell}{\ub} \cdot \delta \ub +  \hat \rho^n\nabla \fder{\ell}{\hat \rho^n} \cdot \etab \right)\tn d\xb\,\tn d t\,,
 \end{align*}
 where we inserted the variations \eqref{variat:n} of $n$-forms and integrated by parts.
 The first term yields the Euler-Poincaré equations \eqref{eulpoinc}, while the second term will add an additional term (it is also multiplied by $\etab$) to these equations:
 $$
 \dt{} \fder{\ell}{\ub} + \nabla \cdot \left( \ub \otimes \fder{\ell}{\ub}\right) + \nabla \ub \cdot \fder{\ell}{\ub} = \hat \rho^n\nabla \fder{\ell}{\hat \rho^n}\,.
 $$
 The functional derivatives of $\ell$ read
 $$
 \fder{\ell}{\ub} = \hat \rho^n \ub\,,\qquad \fder{\ell}{\hat\rho^n} = \frac{|\ub|^2}{2} - V\,,
 $$
 which leads to
 \be \label{cancel}
 \dt{} (\hat \rho^n \ub) + \nabla \cdot \left( \hat \rho^n \ub \otimes \ub\right) + \hat \rho^n \nabla \ub \cdot \ub = \hat \rho^n \nabla \ub \cdot \ub - \hat \rho^n \nabla V\,.
 \ee
 There is a cancellation of terms left and right; after adding the transport equation for $\hat \rho^n$ (which was assumed from the start), we obtain the system
 \begin{align*}
  &\pder{\hat \rho^n}{t} + \nabla \cdot (\hat \rho^n \ub) = 0\,,
  \\[2mm]
  &\pder{ (\hat \rho^n \ub)}{t} + \nabla \cdot \left( \hat \rho^n \ub \otimes  \ub\right) = - \hat \rho^n \nabla V\,.
 \end{align*}
 These are the classical Euler equations for a cold fluid (no pressure) in a central force field $-\nabla V$. Noting that 
 \be
 \nabla \cdot \left( \hat \rho^n \ub \otimes \ub\right) = \nabla \cdot \left( \rho^n \ub \right)  \hat \ub + \rho^n \ub \cdot \nabla \ub\,,
 \ee
 and using the conservation law of $\hat \rho^n$, the system can also be written as
 \begin{align*}
  &\pder{\hat \rho^n}{t} + \nabla \cdot (\hat \rho^n \ub) = 0\,,
  \\[2mm]
  &\pder{ \ub}{t} + \ub \cdot \nabla  \ub = - \nabla V\,.
 \end{align*}
\end{example}






