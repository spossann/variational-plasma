\section{Lecture 4: \emph{``Manifolds''}}

\subsection{General definition}
 
 \emph{Manifolds} are the central objects in the field of differential geometry. They provide a generalization of familiar concepts such as curves and surfaces in $\RR^3$. Manifolds are used to study the "geometric" properties of objects, i.e. properties that are independent of the choice of a local coordinate system. However, by definition such a local coordinate system can always be chosen, and for practical applications it often is. This means that whenever one hears the word "manifold", one could replace "open subset of Euclidean space" without loosing too much generality. 

 \begin{definition}
  {\bf (Manifolds.)} An \emph{$m$-dimensional manifold} is a set $M$, together with a countable collection of subsets $U_\alpha \subset M$, called \emph{coordinate charts}, and one-to-one mappings $\chi_\alpha: U_\alpha \to \Omega_\alpha$ onto connected open subsets $\Omega_\alpha \subset \RR^m$, which satisfy
  \begin{enumerate}
   \item $\bigcup_\alpha U_\alpha = M$
   \item For any intersection $U_\alpha \cap U_\beta$, the composite mapping
   $$
   \chi_\beta \circ \chi_\alpha^{-1}:\quad \chi_\alpha(U_\alpha \cap U_\beta) \to \chi_\beta(U_\alpha \cap U_\beta)
   $$
   is smooth (infinitely differentiable).
  \end{enumerate}
\end{definition}

\noindent
The number and choice of charts for a manifold are not unique.
Let us state some examples:
\begin{itemize} 
 \item $\RR^m$ is an $m$-dimensional manifold with itself as the single coordinate chart under the identity mapping.
 \item Any open subset of $\RR^m$ is an $m$-dimensional manifold with itself as the single coordinate chart under the identity mapping.
 \item {\bf Remark:} Any manifold $M$ with a single coordinate chart defined by $\chi:M \to \Omega \subset \RR^m$ can be identified with its image $\Omega$.
 \item The unit sphere $S^2 = \{(x, y, z) \in \RR^3 \ : \ x^2 + y^2 + z^2 = 1\}$ is a 2-dimensional manifold defined by two coordinate charts, e.g. the stereographic projections from the poles.
 \item The unit circle $S^1 = \{(x, y) \in \RR^2 \ : \ x^2 + y^2 = 1\}$ is a 1-dimensional manifold that can be defined by at least two coordinate charts.
 \item The $m$-dimensional Torus $T^m = S^1 \times \ldots \times S^1$ is an $m$-dimensional manifold.
 \item {\bf Remark:} If $M$ and $N$ are smooth manifolds of dimension $m$ and $n$, respectively, then their Cartesian product $M \times N$ is a smooth manifold of dimension $m + n$.
 \item The set of real-valued $n\times n$ matrices $\mathcal M(n)$ is an $n^2$-dimensional manifold, because it can be identified with $\RR^{n^2}$.
 \item The set of invertible $n\times n$ matrices $GL(n)$ is an $n^2$-dimensional manifold, because it is an open subset of $\mathcal M(n)$.
 \item The set of orthogonal $n\times n$ matrices $O(n)$ ($A^\top A = I$) is an $n(n-1)/2$-dimensional manifold.
 \item The set of diffeomorphisms $\Phi: \Omega \to \Omega$ on $\Omega\subset \RR^m$ is an infinite-dimensional manifold. These have some additional technicalities associated with them, which we will not address during this lecture.
\end{itemize}

A {\bf local change of coordinates} is when we compose a local chart map $\chi_\alpha: U_\alpha \to \Omega_\alpha$ with a diffeomorphism\footnote{A diffeomorphism is a smooth (infinitely differentiable) function with smooth inverse.} $\psi:\Omega_\alpha \to \hat \Omega_\alpha \subset \RR^m$. An example of this would be to go from Cartesian to polar coordinates on the unit disk, which is a 2-dimensional manifold with a single chart.

If $M$ and $N$ are smooth manifolds, a {\bf map $F:M\to N$ between manifolds} is \emph{smooth} if its coordinate representation is a smooth map in any chart. In other words, suppose $\chi_\alpha:U_\alpha \to \Omega_\alpha$ and $\chi_\beta:\hat U_\beta \to \hat\Omega_\beta$ are two charts on the respective manifolds, then the composite mapping
$$
 \psi_\beta \circ F \circ \chi_\alpha^{-1}: \Omega_\alpha \to \hat \Omega_\beta
$$
is smooth wherever it is defined.
 
 
 
\subsection{Submanifolds of $\RR^n$}

 It is worthwhile studying submanifolds of $\RR^n$ because many abstract sets (such as matrix groups) map to Euclidean space. Hence the study of such abstract sets is facilitated by an understanding (embedded) submanifolds in $\RR^n$.
 
 Recall that the graph of a function $f:\RR\to\RR$ is given by the set $\{(x,f(x)) \in \RR^2 \ : \ x \in \RR\}$. Similarly, the graph of a function $f:\RR^2 \to \RR$ could be written as $\{(x,y,f(x, y)) \in \RR^3 \ : \ (x, y) \in \RR^2\}$. Such definitions can be written down in higher dimensions. {\bf A submanifold is the union of one or more graphs.} A bit more precise:
 
 \begin{definition}
  An {\bf $m$-dimensional, embedded submanifold of $\RR^n$} is a subset $M \subset \RR^n$ such that, for every $\ab \in M$ there is a neighbourhood\footnote{A neighbourhood $B_\ab$ is an open set containing $\ab$.} $B_\ab$ such that $M \cap B_\ab$ is the graph of some smooth function with an open domain, expressing $n-m$ coordinates in terms of the other $m$. The \emph{codimension} of $M$ is $n -m$.
 \end{definition}
 
 \begin{example}
  The circle $S^1$ is a 1-dimensional submanifold of $\RR^2$, because the top and bottom semi-circles are graphs of the functions $y=\pm\sqrt{1-x^2}$, with domain $(-1, 1)$, and the left- and right semi-circles are graphs of $x = \pm\sqrt{1-y^2}$, with domain $(-1,1)$.
 \end{example}
 
 There are three different ways of defining a submanifold of $\RR^n$: {\bf by the union of graphs, by level sets, or by parametrization}. In the previous example, the unit circle $S^1$ has been characterized by the union of four graphs. Moreover, we could say that $S^1 = \{(x, y) \in \RR^2 \ : \ x^2 + y^2 = 1\}$, which is the definition through a level set of the function $f(x, y) = x^2 + y^2$. As a third option, the unit circle can be said to be the image of the mapping $(0, 2\pi) \to \RR^2$ defined by $\theta \mapsto (\cos\theta, \sin\theta)$, which is a parametrization of the circle. Let us put this on more formal grounds.
 
 Consider a function $f:\RR^n \to \RR$. The set of points $\xb \in \RR^n$ which satisfy $f(\xb) = c$ for some given $c\in \RR$ is called a \emph{level set} of $f$. We denote this set by $f^{-1}(c)$, called the \emph{pre-image} of $c$ (under $f$). This set is an $n-1$-dimensional submanifold of $\RR^n$ if $\nabla f(\xb) \neq 0$ for all $\xb \in f^{-1}(c)$. More generally, in case of multiple constraints $f_i(\xb) = c_i$, we can think of $f = (f_1,\ldots,f_k)$ as a mapping $f: (A\subset \RR^n) \to \RR^k$. The \emph{Jacobian matrix} of this mapping is defined as
 \be \label{jacobian}
 Df: A \to \RR^{k \times n}\,,,\qquad [Df]_{ij}(\xb) = \pder{f_i(\xb)}{x_j}\,,
 \ee
 such that $\nabla f_i(\xb)$ is the $i$-th row of the matrix at $\xb$. The Jacobian is surjective\footnote{Surjective means that the image of a linear map is the whole co-domain, in this case $\RR^k$.} at $\xb$ if rank$(Df(\xb)) = k$ (for $n \geq k$). The relation to manifolds is made by the following statements:
 
 \begin{theorem}
  {\bf (Implicit Function Theorem.)}
  Let $A$ be an open subset of $\RR^n$ and let $f : A \to \RR^k $ be a smooth function. Let $\xb \in A$ and let $f (\xb) = \cb$ for a given $\cb \in \RR^k$. If $Df(\xb)$
{\bf is surjective}, then there exists a neighbourhood $B_\xb$ such that $f^{-1}(\cb) \cap B_\xb$ is the
graph of some smooth function expressing $k$ of the standard variables $x_1 , \,\ldots , x_n$ in terms of the others.
 \end{theorem}

\begin{corollary}
 If the function $f: (A\subset \RR^n) \to \RR^k$ is smooth and if $\cb \in \RR^k$ is such that $Df(\xb)$ is surjetive for all $\xb \in f^{-1}(\cb)$, then $f^{-1}(\cb)$ is a $n-k$-dimensional submanifold of $\RR^n$, of codimension $k$.
\end{corollary}

\begin{example}
 Let $S^1 = f^{-1}(1)$, where $f(x, y) = x^2 + y^2$. We have $\nabla f = (2x, 2y)$ which is non-zero away from the origin. Hence, $Df(x,y)$ is surjective for $(x,y) \neq (0,0)$ and thus $S^1$ is a 1-dimensional submanifold of $\RR^2$.
\end{example}

For the parametric definition of manifolds, let us consider functions $\psi: (A \subset \RR^m) \to \RR^n$ where $m \leq n$.  The Jacobian matrix $D\psi(\ab) \in \RR^{n \times m}$ of such functions is injective\footnote{Injective means that the kernel of a linear map is just the null element.} at $\ab \in A$ if rank$(D\psi(\ab)) = m$. 

\begin{theorem}
 {\bf (Parametrized manifolds)} Suppose $\psi: (A \subset \RR^m) \to \RR^n$ is such that $D\psi(\ab)$ is injective for all $\ab \in A$ and that the inverse function $\psi^{-1}:\psi(A) \to A$ is continuous. Then $\psi(A)$ is an $m$-dimensional submanifold of $\RR^n$.
\end{theorem}

\begin{example}
 Let $\psi: \RR \to \RR^2$ be given by $\theta \mapsto (\cos\theta, \sin\theta)$. Since $\psi'(\theta) = (-\sin\theta, \cos\theta)$ is never $(0,0)$, $D\psi(\theta)$ is injective for all $\theta \in \RR$. However, $\psi$ does not have an inverse on $\RR$, but its restriction to $(0,2\pi)$ does have a continuous inverse. Hence, the image of $\psi: (0, 2\pi) \to \RR^2$ defines a 1-dimensional submanifold of $\RR^2$, the unit circle.
\end{example}



\subsection{Tangent spaces}

Consider a point particle moving on a submanifold $M$. Let $\xb = \Phi(t)$ denote the position of the particle at time $t$ on $M$, where $\Phi:\RR \to M$ is a parametrization of the particle's trajectory in $M$. Then $\vb = \dot \Phi(t)$ is its velocity vector, or "tangent vector" to the curve $\Phi$ at time $t$. The set of all possible tangent vectors at $\xb$, corresponding to all possible parametrized curves through $\xb$, is the \emph{tangent space} at the base point $\xb$. This is the generalization of a tangent line to a graph or a tangent plane to a surface at a point $\xb$, with one important difference: {\bf the tangent space contains the zero element, and is thus a vector space} (which we shall prove below).

\begin{definition}
 {\bf (Tangent space.)} Let $M$ be a submanifold of $\RR^n$. A {\bf tangent vector} at $\xb \in M$ is $\vb = \dot \Phi(0)$ for some parametrized curve $\Phi: \RR\to M$ with $\Phi(0) = \xb$. The set of all tangent vectors at $\xb$ is called the {\bf tangent space} at the base point $\xb$, denoted by $T_\xb M$. 
\end{definition}

The base point is sometimes included in the definition:
 \be \label{tangentspace}
 T_\xb M = \{(\xb,\vb) \ : \ \vb = \dot\Phi(0) \tn{ for some curve $\Phi$ in $M$ with $\Phi(0) = \xb$}\}\,. 
 \ee 
This makes sense for the following definition:

\begin{definition}
 {\bf (Tangent bundle.)} The {\bf tangent bundle} of a submanifold $M\subset \RR^n$, denoted by $TM$, is the union of all tangent spaces to $M$: 
 $$
 TM = \bigcup_\xb \, T_\xb M\,.
 $$
\end{definition}

\begin{example}
 If $M\subset \RR^n$ is open, then $TM = M \times \RR^n$. This is because any $\vb \in \RR^n$ can be obtained as $\vb = \dot\Phi(0)$ with $\Phi(t) = \xb + t \vb$.
\end{example}

Depending on how the manifold was defined (through graphs, level set or parametrized), there are different ways of computing the tangent spaces. Because of their practical significance, in what follows let us state the computation of parametrized and of level set manifolds.

\begin{theorem} \label{thm:txm:param}
 {\bf (Tangent spaces of parametrized manifolds.)} Suppose $M$ to be an $m$-dimensional submanifold of $\RR^n$, parametrized by the map $\psi:(A\subset \RR^m) \to \RR^n$ such that $\psi(\ab) = \xb$. Then,
 $$
 T_\xb M = \tn{Im}\ D\psi(\ab)\,,\qquad \tn{dim}\ T_\xb M = m\,.
 $$
 i.e. the image of the Jacobian matrix at $\ab \in A$, which is a vector space of dimension $m$. 
 
 \begin{proof}
  Every tangent vector at $\xb \in M$ is given by $\dot \Phi(0)$ for some curve $\Phi:\RR \to M$ with $\Phi(0) = \xb$. Define $\alpha(t) = \psi^{-1} \circ \Phi(t)$ to be the corresponding curve in $A \subset \RR^m$. We have $\alpha(0) = \ab$,  $\Phi(t) = \psi \circ \alpha(t)$ and
  $$
  \dot \Phi(0) = D\psi (\ab) \, \dot \alpha(0)\,.
  $$
  Since $\alpha$ in an arbitrary path in $A$ open, the vector $\dot \alpha(0)$ can take any value in $\RR^m$. Moreover, $D\psi(\ab) \in \RR^{n \times m}$ is injective (rank $m$), which proves the result.
 \end{proof}
\end{theorem}

\begin{example}
 Consider the unit circle $S^1$ with the parametrization $\psi: (0, 2\pi) \to S^1$ where $\psi(\theta) = (\cos\theta, \sin \theta)$. At a general point $\xb = (x,y) = \psi(\theta)$ we have
 $$
 D\psi(\theta) = \begin{bmatrix}
                  -\sin\theta \\ \cos\theta
                 \end{bmatrix} = 
                 \begin{bmatrix}
                  -y \\ x
                 \end{bmatrix}\,.
 $$
 Therefore,
 $$
  T_{(x,y)} S^1 = \tn{Im}\, D\psi(\theta) = \{(-\lambda y, \lambda x) \ : \ \lambda \in \RR\}\,.
 $$
\end{example}

\begin{theorem}
 {\bf (Tangent spaces of level set manifolds.)} Suppose $M$ to be an $m$-dimensional submanifold of $\RR^{m+k}$, given as the pre-image $f^{-1}(\cb)$ of $\cb \in \RR^k$ under some function $f: \RR^{m+k} \to \RR^k$. Then,
 $$
 T_\xb M = \tn{Ker} D f(\xb) = \{\vb \in \RR^{m+k} \ : \ \nabla f_i(\xb) \cdot \vb = 0\,, \ i=1,\ldots,k\}\,.
 $$
 
 \begin{proof}
  Every tangent vector at $\xb \in M$ is given by $\vb = \dot \Phi(0)$ for some curve $\Phi:\RR \to M$ with $\Phi(0) = \xb$. Since $f(\Phi(t)) = \cb$ we have
  $$
  \dt{} \Big |_{t=0} f(\Phi(t)) = Df(\xb)\, \vb = 0\,, 
  $$
  whichs shows that $T_\xb M \subset \tn{Ker} D f(\xb)$. Moreover, Theorem \ref{thm:txm:param} showed that $\tn{dim}\ T_\xb M = m$. Since $Df(\xb) \in \RR^{k \times (m+k)}$ is surjective (rank $k$), the dimension of its kernel is $(m+k) - k = m$.  Hence $T_\xb M$ and $\tn{Ker} D f(\xb)$ have the same dimension and because one is a subset of the other, they must be equal.
 \end{proof}
\end{theorem}

\begin{example}
 Consider the sphere $S^2 = \{(x,y,z) \in \RR^3 \ : \ x^2 + y^2 + z^2 = 1\}$, defined as the pre-image of $c=1$ under the map $f = x^2 + y^2 + z^2$. The Jacobian matrix reads $Df(x,y,z) = (2x,2y,2z)$, and hence the tangent space is
 $$
 T_{(x,y,z)} S^2 = \{(u,v,w) \in \RR^3 \ : \ ux + vy + wz = 0\}\,.
 $$
\end{example}

From Theorem \ref{thm:txm:param} we know that, for an $m$-dimensional submanifold $M$, the tangent space $T_\xb M$ at $\xb \in M$ is a vector space of dimension $m$. This leads to the following convention for identifying elements of the tangent bundle $TM$: if $\qb \in A \subset \RR^m$ are some coordinates in $M$, i.e. there is some parametrization $\psi: (A \subset \RR^m) \to M$, $\xb = \psi(\qb)$, then $\dot \qb \in \RR^m$ identifies one element in $T_\xb M$. Formally, one writes $(\qb,\dot \qb) \in TM$, but indeed what is meant is the correspondence
\be \label{abuse}
 T_\xb M \ni (\xb, \vb) = (\psi(\qb), D\psi(\qb) \dot \qb)\,.
\ee
The last relation is also frequently called the \emph{tangent lift} of the differentiable map $\psi$. We need one last definition for maps between manifolds:

\begin{definition}
 {\bf (Tangent map.)} Let $f: M \to N$ be a smooth map between manifolds. The {\bf tangent map} $T_\xb f$ of $f$ at $\xb \in M$ is defined as
 $$
  T_\xb f: T_\xb M \to T_{f(\xb) }N\,,\qquad \vb \mapsto \dt{} \bigg |_{t=0} f(\Phi(t))\,,
 $$
 where $\Phi(t)$ is a path in $M$ such that $\Phi(0) = \xb$ and $\dot\Phi(0) = \vb$. The tangent map at $\xb$ is also called the {\bf derivative of $f$} at $\xb$ and denoted by $Df(\xb)$ or $f_*(\xb)$. Is is independent of the chosen path $\Phi$.
\end{definition}



