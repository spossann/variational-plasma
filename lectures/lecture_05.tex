\section{Lecture 5: \emph{``Differential forms''}} \label{sec:forms}

Differential forms provide a unified approach to integrands over curves, surfaces, volumes, and higher-dimensional manifolds. We shall not attempt to give an in-depth view on differential forms here, but rather focus on the notions needed to describe the variational principles in the later chapters. We refer to the excellent books \cite{frankel, tu, marsden}, among many others, for further reading.

\subsection{Line integrals: co- and contravariant representation}

In what follows we assume $\Omega \subset \RR^n$ to be open and denote by $\xb\in \Omega$ a point in standard, Cartesian coordinates (the chart map is the identity.). Let us start with the integral of a vector field $\Eb:\Omega \to\RR^n$ along a curve $C:\RR \supset I \to \Omega$, which is usually written as
\be \label{lineint}
 \int_C \Eb \cdot \tn d \xb = \int_C E_i\, \tn d x^i\,,
\ee
where we assume the Einstein sum convention over repeated indices. The integrand can be viewed as a linear combination of the differentials $\tn dx^i$. As we shall see below, in Cartesian coordinates $\xb = (x_i)_i \in \Omega$ the differentials are just the canonical basis vectors $\tn d x^i_j = \delta_{ij}$ and the $(E_i)_{i=0}^n$ are the components of the vector field $\Eb$ in Euclidean space.

\begin{definition}
    \textbf{(Differential.)} Suppose $x = x(t)$ is a scalar-valued, differentiable function, then its differential is a function of two variables, linear in the second variable, defined as
    $$
     \tn dx(t, \Delta t) := \dot x(t) \Delta t = \dt{x} \tn d t\,,
    $$
    where $\tn d t = \Delta t$ was used to get the second equality.
\end{definition}

The differential is a local, linear approximation of $x(t)$ such that
$$
 x(t + \Delta t) = x(t) + \tn dx(t, \Delta t) + O(\Delta t^2)\quad \tn{as} \ \Delta t \to 0\,.
$$
This notion can be generalized to multivariate functions $x = x(\eta_1,\ldots,\eta_n)$, where the differential is given by
\be \label{dx}
 \tn d x = \pder{x}{\eta_i} \tn d \eta^i \qquad \tn{(Einstein sum convention)}\,.
\ee

Coming back to the line integral \eqref{lineint}, let us suppose that the curve $C$ is given by a parametrization $C =\xb(t) = (x_i(t))_i$ for $t \in (a,b) = I$, then the transformation of the differentials gives the standard formula for the line integral:
\be \label{lineint:2}
 \int_C E_i\, \tn d x^i = \int_a^b E_i(\xb(t)) \dt{x_i}\, \tn dt = \int_a^b \Eb(\xb(t)) \cdot \dt{\xb}\,\tn d t  \,.
\ee 

Let us see what happens if we express the line integral \eqref{lineint} in a different set of (curvilinear) coordinates $\etab = (\eta_i)_i \in \hat\Omega \subset \RR^n$ open, given by a diffeomorphism $F:\hat\Omega \to \Omega$, $\etab \mapsto F(\etab) = \xb$, with Jacobain matrix $DF:\hat\Omega \to \RR^{n\times n}$, its volume element $\sqrt g: \hat\Omega \to \RR$ and the metric tensor $G:\hat\Omega \to \RR^{n\times n}$ defined by
$$
 DF_{i,j} = \pder{F_i}{\eta_j}\,,\qquad \sqrt g = |\tn{det}(DF)|\,,\qquad G = DF^\top DF\,.
$$
For instance, $F$ could be chosen such that the integration curve $C$ parametrizes one of the coordinate lines. In any case, let us write the curve $C = F(\hat C)$ is the image of a curve $\hat C:(a,b) \to \hat\Omega$. Then, according to the transformation rule \eqref{dx} of differentials,
$$
 \int_{F(\hat C)} E_i(\xb)\, \tn dx^i = \int_{\hat C}E_i(F(\etab)) \pder{F_i}{\eta_j} \tn d \eta_j =: \int_{\hat C} \hat E_j^1\, \tn d \eta^j\,.
$$
Here, the vector $\hat \Eb^1 = (\hat E_j^1)_j$ with lower indices $j$, defined by
\be \label{pull:1}
 \hat \Eb^1(\etab) := DF^\top \Eb(F(\etab))\,,
\ee
is called the \emph{covariant} representation of $\Eb$ in the curvilinear coordinates defined by $F$. Equation \eqref{pull:1} is also the transformation rule, or \emph{pullback} to $\hat\Omega$, of 1-forms. 

\begin{definition}
 {\bf (Differential $p$-forms - formally.)} A differential $p$-form is what can be integrated over a $p$-dimensional submanifold of $\RR^n$, where $0 < p\leq n$. The \emph{coordinate representation} or \emph{components} of a $p$-form are defined such that the integral appears independent of the metric.
 \end{definition}

Hence, 1-forms $\Eb \cdot \tn d \xb$ are the objects that can be integrated along lines or curves $C$, which are 1-dimensional submanifolds of $\RR^n$. With the transformation rule (pullback) of 1-form components $\Eb$ defined by \eqref{pull:1}, the metric (Jacobian matrix) never appears in the integrand, no matter what the coordinates are. To repeat:
\be \label{lineint:3}
 \int_{F(\hat C)} \Eb \cdot \tn d\xb = \int_{\hat C} DF^\top \Eb  \cdot \tn d \etab = \int_{\hat C} \hat \Eb^1 \cdot \tn d \etab = \int_\gamma E^1\,.
\ee
The last term is the purely geometric representation of the line integral, completely independent of the chosen coordinates ($\gamma$ is the curve as a geometric object, without coordinates describing it).\\

There is another representation of the vector field $\Eb$ in curvilinear coordinates, called the \emph{contravariant} representation, which is dual to 1-forms. Let us define the columns of the Jacobian $DF$ as
$$
\pa_j := \pder{F}{\eta_j} \ : \ \hat\Omega\to\RR^n\,.
$$
Since $F$ is invertible, the columns $\pa_i$ of $DF$ are linearly independent, and hence form a basis at each point $\etab \in \hat\Omega$. The representation of a vector-valued function $\Eb:\Omega \to \RR^n$ in this basis reads
\be \label{pull:vec}
 \Eb \circ F = E^i\pa_i = DF \hat \Eb\,.
\ee
The components $\hat \Eb = (E^i)_i$ with upper index are called the \emph{contravariant} components of $\Eb$. Contrast this to the covariant representation
$$
\Eb \circ F = E_i \,\tn d \eta^i = DF^{-\top}\hat \Eb^1\,,
$$
from which follows that $\tn d \eta^i$ is the $i$-th line of the inverse Jacobian. Since $DF^{-1}DF = \mathds 1$, it follows that
\be \label{dual}
 \tn d \eta^i \cdot \pa_j = \delta_{ij}\qquad \forall \ i,j\,.
\ee
This means that the differentials $(\tn d \eta^i)_i$ are in fact the \emph{dual basis} to the columns $(\pa_i)_i$ of the Jacobian.
Moreover, it is now clear how the co- and contravariant components of a vector can be easily obtained form the Cartesian representation:
\begin{align}
 \Eb \cdot \pa_j &= E_i \,\tn d \eta^i \cdot \pa_j = E_j\,,\\[1mm]
 \Eb \cdot \tn d \eta^j &= E^i\pa_i \cdot \tn d \eta^j = E^j \,.
\end{align}
\begin{example}
 Let us consider $\RR^3$. Given three linearly independent vectors $(\pa_i)_i$, there is a standard way of constructing the dual basis, namely
\be \label{dual:3d}
 \tn d \eta_1 = \frac{\pa_2 \times \pa_3}{\pa_1 \cdot(\pa_2 \times \pa_3)}\,,\quad \tn d \eta_2 = \frac{\pa_3 \times \pa_1}{\pa_2 \cdot(\pa_3 \times \pa_1)}\,,\quad \tn d \eta_3 = \frac{\pa_1 \times \pa_2}{\pa_3 \cdot(\pa_1 \times \pa_2)}\,.
\ee
It is easily seen that \eqref{dual} is indeed satisfied. Moreover, we note for the Jacobian determinant 
$$
 J = \pa_1 \cdot(\pa_2 \times \pa_3) = \pa_2 \cdot(\pa_3 \times \pa_1) = \pa_3 \cdot(\pa_1 \times \pa_2)\,.
$$
\end{example}

In case that $\xb = F(\etab) = \etab$ is the identity, the basis vectors $(\pa_i)_i$ and $(\tn d \eta^i)_i$ are just the canonical basis vectors and co- and contravariant representations of a vector $\Eb$ are identical. In terms of the contravariant components $\hat \Eb = G^{-1} \hat \Eb^1$, the line integral \eqref{lineint:3} would read
$$
 \int_{F(\hat C)} E_i\, \tn d x^i = \int_{\hat C} (G \hat \Eb)_i\, \tn d \eta^i \,.
$$
The appearance of the metric tensor $G$ makes this an "un-geometric" representation of the line integral, which depends on the chosen coordinates.

\subsection{(Hyper)surface- and volume integrals}

Thus far, we learned that line integrals are over 1-forms and how the 1-form representation in curvilinear coordinates can be obtained. Moreover, in Definition \ref{def:formal} we saw that $p$-forms have been defined to be the natural or geometric (= independent of the metric) representation for integrals over $p$-dimensional submanifolds of $\RR^n$. For example, in $\RR^3$, surface integrals are over 2-forms and volume integrals are over 3-forms. Let us confirm this in what follows.

\begin{definition}
    \textbf{(Exterior product of 1-forms.)} Let $T^*_{\etab}\hat\Omega = \tn{span}(\tn d\eta^i(\etab))$ denote the $n$-dimensional vector space spanned by the basis differentials at point $\etab \in \hat\Omega$. The exterior product of two 1-forms at $\etab$ is a linear map that satisfies 
    \begin{align*}
        1.\qquad & a\wedge a = 0\qquad a \in T^*_{\etab}\hat\Omega\,,
        \\[2mm]
        2.\qquad & (a + b) \wedge c = a \wedge c + b \wedge c \qquad a,b,c\in T^*_{\etab}\hat\Omega\,.
    \end{align*}
\end{definition}

The above definition immediately yields anti-symmetry $a\wedge b = -b\wedge a $ since
$$
 0 = (a + b) \wedge (a + b) = a \wedge b + b \wedge a\,.
$$

\begin{example}
    For a three-dimensional manifold $\hat\Omega \subset \RR^3$ we have $T^*_{\etab}\hat\Omega = \RR^3$ and the exterior product of two 1-forms reads
    \be \label{wedge:2}
    \begin{aligned}
    \sum_{i=1}^3 a_i\, \tn d \eta^i \wedge \sum_{j=1}^3 b_j\, \tn d \eta^j &= \sum_{i=1,j=1}^{3,3} a_i b_j\, \tn d \eta^i \wedge \tn d \eta^j
    \\
    &=(a_2 b_3 - a_3 b_2) \,\tn d\eta^2 \wedge \tn d\eta^3
    \\
    &  + (a_3 b_1 - a_1 b_3) \,\tn d\eta^3 \wedge \tn d\eta^1 
    \\
    & + (a_1 b_2 - a_2 b_1) \,\tn d\eta^1 \wedge \tn d\eta^2\,.
    \end{aligned}
    \ee
    In terms of the coefficients $\ab=(a_i)_i$, $\bb=(b_i)_i$ this just the cross product $\ab\times \bb$ of two vectors. 
\end{example}

 We denote by $V_{\etab}^2$ the vector space defined as the span of the exterior products of the basis differentials at $\etab \in \hat\Omega$,
$$
 V_{\etab}^2 := \tn{span}(\tn d\eta^i(\etab) \wedge \tn d \eta^j(\etab)) \,.
$$
Because of anti-symmetry, $\tn{dim}\, V_{\etab}^2 = \binom{n}{2}$. In the above example, $\tn{dim}\, V_{\etab}^2 = 3$ and the basis is
$$
V_{\etab}^2 = \tn{span}(\tn d \eta^2 \wedge \tn d\eta^3,\ \tn d\eta^3 \wedge \tn d\eta^1,\ \tn d\eta^1 \wedge \tn d\eta^2)\,.
$$
The exterior product can be extended to multiple $a_j \in T^*_{\etab}\hat\Omega$ to be the distributive product that satisfies
\begin{align}
    &a_1 \wedge a_2 \wedge \ \ldots \ \wedge a_p = \tn{sgn}(\sigma) \ a_{\sigma(1)} \wedge a_{\sigma(2)} \wedge \ \ldots \ \wedge a_{\sigma(p)} \,,
\end{align}
where $p>1$, $\sigma$ is a permutation of $(1,\ldots,p)$ and $\tn{sgn}(\sigma)$ is the signature of the permutation (=minimum number of transpositions to get $\sigma$). This implies 
\be \label{wedgezero}
 a_i = a_j  \tn{ for any } i \neq j \quad \implies \quad a_1 \wedge a_2 \wedge \ \ldots \ \wedge a_p = 0\,.
\ee
\begin{example}
 Let again $T^*_{\etab}\hat\Omega = \RR^3$, then the exterior product of three 1-forms reads
\be \label{wedge:3}
\begin{aligned}
\sum_{i=1}^3 a_i\, \tn d \eta^i &\wedge \sum_{j=1}^3 b_j\, \tn d \eta^j \wedge \sum_{j=1}^3 c_k\, \tn d \eta^k = \sum_{i=1,j=1,k=1}^{3,3,3} a_i b_j c_k\, \tn d \eta^i \wedge \tn d \eta^j \wedge \tn d \eta^k
\\[2mm]
&=(a_1 b_2 c_3 - a_1 b_3 c_2 + a_2 b_3 c_1 - a_2 b_1 c_3 + a_3 b_1 c_2 - a_3 b_2 c_1) \,\tn d\eta^1 \wedge \tn d\eta^2 \wedge \tn d \eta^3\,.
\end{aligned}
\ee
In terms of the coefficients $\ab=(a_i)_i$, $\bb=(b_i)_i$, $\cb=(c_i)_i$ this just the triple product $\ab \cdot \bb \times \cb$ of three vectors.
\end{example}

We thus define in general
$$
 V_{\etab}^p := \tn{span}(\tn d\eta^{i_1}(\etab) \wedge \ \ldots \ \wedge \tn d \eta^{i_p} (\etab))\,,\qquad 0 < p \leq n \,.
$$
Because of anti-symmetry, $\tn{dim}\, V_{\etab}^p = \binom{n}{p}$. In the above example,$\tn{dim}\, V_{\etab}^3 = 1$ and the basis is
$$
 V_{\etab}^3 = a\,\tn d\eta^1 \wedge \tn d\eta^2 \wedge \tn d \eta^3\qquad a \in \RR\,.
$$

\begin{definition}
    \textbf{(Differential $p$-form).} Let $V^0 = \hat\Omega$ and $V^p=\bigcup_{\etab}V_{\etab}^p$ for $0<p\leq n$ ($V^1 = T^*\hat\Omega$ is called the cotangent bundle). A differential $p$-form is an element $a^p \in V^p$ such that the coefficient function $\hat a^p(\etab)$ is differentiable in $\hat\Omega$.
\end{definition}

\begin{example}
Let $\hat \Omega =\Omega = \RR^3$ be the three-dimensional Euclidean space. In this case the basis differentials $(\tn d x^i)_i$ are the canonical unit vectors, and we have 4 different $p$-forms, $0 \leq p \leq 3$,
\begin{align*}
    V^0 \ni p^0 &= p(\xb)\,,
    \\[2mm]
    V^1 \ni E^1 &= E_i(\xb) \,\tn d x^i\,,
    \\[2mm]
    V^2 \ni E^2 &= E_1(\xb) \,\tn d x^2 \wedge \tn d x^3 + E_2(\xb) \,\tn d x^3 \wedge \tn d x^1 + E_3(\xb) \,\tn d x^1 \wedge \tn d x^2\,,
    \\[2mm]
    V^3 \ni p^3 &= p(\xb) \,\tn d x^1 \wedge \tn d x^2 \wedge \tn d x^3\,.
\end{align*}
In the above notation, we chose the coefficient function $p(\xb)$ to be the same in the 0-form and in the 3-form; like-wise, the coefficient functions $\Eb=(E_i)_i$ are the same in the 1-form and in the 2-form. This is not by accident. Indeed, below we will show that in Euclidean space this representations are equivalent (because there is no metric).
\end{example}

In what follows, we shall assume $\Omega \subset \RR^3$ open. Let us consider surface and volume integrals in $\RR^3$. We can start with the claim that the integral of a vector field over a parametrized surface $S:\RR^2 \supset R \to \Omega$ can be written as
\be \label{surfint}
 \int_S \Eb \cdot \tn d \mathbf s = \int_S \left( E_1 \,\tn d x^2 \wedge \tn d x^3 + E_2 \,\tn d x^3 \wedge \tn d x^1 + E_3 \,\tn d x^1 \wedge \tn d x^2 \right) =  \int_S E^2\,,
\ee
where $\tn d \mathbf s$ is the surface element. Given a parametrization of the surface $S = \xb(s,t)$ with $(s,t) \in (s_1,s_2)\times (t_1,t_2) =R$, the transformation rule  of differentials \eqref{dx} yields
\begin{align*}
 \int_S \Eb \cdot \tn d \mathbf s = \int_{s_1}^{s_2}\int_{t_1}^{t_2} &  \left[E_1(\xb(s,t))\left( \pder{x_2}{s}\pder{x_3}{t} - \pder{x_2}{t}\pder{x_3}{s}\right) \right.
 \\
 + &\ \ \left.E_2(\xb(s,t))\left( \pder{x_3}{s}\pder{x_1}{t} - \pder{x_3}{t}\pder{x_1}{s}\right) \right.
 \\
 + &\ \ \left.E_3(\xb(s,t))\left( \pder{x_1}{s}\pder{x_2}{t} - \pder{x_1}{t}\pder{x_2}{s}\right) \right]\,\tn d s \wedge \tn d t \,,
 \\
 = \int_{s_1}^{s_2}\int_{t_1}^{t_2} & \Eb \cdot \left( \pder{\xb}{s} \times \pder{\xb}{t}\right) \tn d s \wedge \tn d t\,,
\end{align*}
which is the classical expression for the surface integral of a vector-field. This shows that the integrand in a surface integral is indeed a 2-form. A nice feature of differential forms is that one can easily compute coordinate transformations, in this case of a surface integral. We just need to compute the pullback of 2-forms, under a diffeomorphism $\xb = F(\etab)$, using the transformation rules of differentials and the wedge product. From $\tn dx^i = \pder{F_i}{\eta_j} \tn d \eta^j$ and equation \eqref{wedge:2} we obtain
$$
\tn d x^i \wedge \tn d x^j = A^{ij}_1\,\tn d\eta^2 \wedge \tn d \eta^3 + A_2^{ij}\,\tn d\eta^3 \wedge \tn d \eta^1 + A_3^{ij}\,\tn d\eta^1 \wedge \tn d \eta^2\,,
$$
with 
$$
A^{ij}_k=\left(\pder{F_i}{\etab} \times \pder{F_j}{\etab}\right)_k\,.
$$
This leads to
\begin{align*}
    E^2 &= E_1(\xb) \,\tn d x^2 \wedge \tn d x^3 + E_2(\xb) \,\tn d x^3 \wedge \tn d x^1 + E_3(\xb) \,\tn d x^1 \wedge \tn d x^2
    \\[2mm]
    &=\underbrace{\left( E_1 A_1^{23} + E_2A_1^{31} + E_3 A_1^{12} \right)}_{=:\hat E^2_1}  \tn d\eta^2 \wedge \tn d \eta^3
    \\[1mm]
    &+\underbrace{\left( E_1A_2^{23} + E_2A_2^{31} + E_2 A_2^{12} \right)}_{=:\hat E^2_2} \tn d\eta^3 \wedge \tn d \eta^1
    \\[1mm]
    &+\underbrace{\left( E_1A_3^{23} + E_2A_3^{31} + E_3A_3^{12} \right)}_{=:\hat E^2_3} \tn d\eta^1 \wedge \tn d \eta^2\,,
\end{align*} 
where we have defined the 2-form component functions $\hat\Eb^2 = (\hat E^2_1, \hat E^2_2, \hat E^2_3)$. We can write the transformation $\Eb \mapsto \hat\Eb^2$ in more compact form:
since $\pder{F_i}{\etab}$ is the $i$-th column of the transpose Jacobian $DF^\top$, from \eqref{dual:3d} we deduce that the column vectors $\Ab^{ij} = (A^{ij}_k)_k$ form the matrix
$$
 (\Ab^{23}|\Ab^{31}|\Ab^{12}) = J\,DF^{-1}\,,
$$
where $J:= \tn{det}(DF)$.
The pullback of a 2-form can thus be compactly written as
\be \label{pull:2}
\begin{aligned}
    \hat \Eb^2(\etab) &= J\,DF^{-1} \Eb(F(\etab))
    \\[1mm]
    &= o(\Omega)\sqrt g\,DF^{-1} \Eb(F(\etab))\,,
\end{aligned}
\ee
where $\sqrt g = |J|$ denotes the volume element and $o(\Omega)$ is the orientation of the manifold. A manifold is said to be "right-handed" ($o(\Omega)=+1$) if $J(\etab)>0$ for all $\etab$ and "left-handed" ($o(\Omega)=-1$) otherwise. Since $J$ is never zero, it is either always positive or always negative. From \eqref{pull:2} we deduce that the sign (or direction) of $\hat\Eb^2$ depends on the orientation of the manifold $\Omega$. The components of a 2-form are thus not a classical vector, but rather a \emph{pseudo-vector}. For a deeper discussion about manifold orientation we refer to \cite{frankel}.

Suppose that the surface $S = F(\hat S)$ is the image of a "logical" surface $\hat S$ under the map $F$. The geometric representation of the surface integral (independent of the metric) is thus
\be \label{surfint:eta}
 \int_{F(\hat S)} \Eb \cdot \tn d \mathbf s = \int_{\hat S} \left( \hat E^2_1 \,\tn d \eta^2 \wedge \tn d \eta^3 + \hat E^2_2 \,\tn d \eta^3 \wedge \tn d \eta^1 + \hat E^2_3 \,\tn d \eta^1 \wedge \tn d \eta^2 \right) =  \int_\sigma E^2\,.
\ee
In the last expression, $\sigma$ is the surface object (which exists without choosing a coordinate system). Comparing the pullback \eqref{pull:2} to the pullback of 1-forms \eqref{pull:1} we find
$$
\hat \Eb^2 = J\,G^{-1}\,\hat\Eb^1\,.
$$
This shows that in right-handed Euclidean space (where the metric is the identity), the component functions of 1-forms and 2-forms are identical; moreover, they are also identical to the "vector-field" components defined in \eqref{pull:vec}. We can summarize these three different representations in a single line:
\be \label{pull:all}
 \Eb \circ F = DF\, \hat\Eb = DF^{-\top} \hat \Eb^1 = \frac{DF}{J} \, \hat \Eb^2\,.
\ee
Finally, let us consider the volume integral over a three form $p^3$ with coefficient function $p$,
\be \label{volint}
\int_V p\,\tn d\xb^3 = \int_V p\,\tn dx^1 \wedge \tn d x^2 \wedge \tn d x^3 = \int_V p^3\,. 
\ee
Let us compute the pullback of the 3-form under the diffeomorphism $\xb = F(\etab)$.  From $\tn dx^i = \pder{F_i}{\eta_j} \tn d \eta^j$ and equation \eqref{wedge:3} we obtain
\be \label{pull:3}
\begin{aligned}
 p\,\tn dx^1 \wedge \tn d x^2 \wedge \tn d x^3 &= p\left[ \pder{F_1}{\etab} \cdot \left( \pder{F_2}{\etab} \times \pder{F_3}{\etab} \right)\right] \tn d \eta^1 \wedge \tn d \eta^2 \wedge \tn d \eta^3
 \\[2mm]
 &= p\,J\, \tn d \eta^1 \wedge \tn d \eta^2 \wedge \tn d \eta^3
 \\[3mm]
 &=: \hat p^3\, \tn d \eta^1 \wedge \tn d \eta^2 \wedge \tn d \eta^3\,.
\end{aligned}
\ee
The pullback of 3-form is thus
\be \label{pull:03}
 \hat p^3(\etab) = J\, p(F(\etab)) = o(\Omega)\sqrt g\, \hat p^0(\etab)\,. 
\ee
In the last equality we also introduced the trivial pullback of 0-forms, $\hat p^0(\etab) = p(F(\etab))$. Suppose that the volume $V = F(\hat V)$ is the image of a "logical" volume $\hat V$ under the map $F$. The geometric representation of the volume integral (independent of the metric) is thus
\be \label{volinteta}
\int_{F(\hat V)} p\,\tn d\xb^3 = \int_{\hat V} \hat p^3\,\tn d\eta^1 \wedge \tn d \eta^2 \wedge \tn d \eta^3 = \int_\nu p^3\,. 
\ee
In the last expression, $\nu$ is the volume object (which exists without choosing a coordinate system).
