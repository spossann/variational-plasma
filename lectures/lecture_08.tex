\section{Lecture 8: \emph{``The Newcomb Lagrangian''}}

The paper of Newcomb \cite{newcomb}, for the first time, states the variational formulation of the MHD equations in both
Lagrangian and Eulerian coordinates. We shall discuss the Eulerian version here, the interested reader can consult the original paper for the details on the Lagrangian formulation (or apply the Euler-Poincaré theorem by himself).
The Newcomb action reads
\be \label{newcombaction}
S(\ub, \hat\rho^3, p, \hat \Bb^2) = \int_0^T\int_{\RR^3} \ell(\ub, \hat\rho^3, p, \hat \Bb^2, \xb)\, \tn d \xb\, \tn d t\,.
\ee
with the Lagrange density
\be
 \ell(\ub, \hat\rho^3, p, \hat \Bb^2, \xb) = \hat \rho^3 \frac{|\ub|^2}{2} - \hat \rho^3 V(\xb) - \frac{p}{\gamma - 1} - \frac{|\hat \Bb^2|^2}{2 \mu_0}\,.
\ee
Here, $V$ is the gravitational potential, $\gamma = 5/3$ is the adiabatic exponent of an ideal gas and $\mu_0$ denotes the magnetic constant. The Newcomb action depends on the plasma velocity $\ub = \dot\Phi_t \circ \Phi_t^{-1}$, where $\Phi_t: \RR^3 \to \RR^3$ is the fluid flow, the plasma mass density $\hat \rho^3$, the magnetic field $\hat \Bb^2$ and the pressure $p$. The Newcomb Lagrange density is just the plasma kinetic energy minus all other energies (potential, internal and magnetic). As indicated by the notation, the mass density is a 3-from and the magnetic field is a 2-form with $\nabla \cdot \hat \Bb^2 = 0$, both assumed to be transported with the fluid flow $\Phi_t$; hence, according to the theorems of section \ref{sec:transp},
\begin{subequations} \label{rhoB:free}
\begin{align}
 &\pa_t \hat \rho^3 + \nabla \cdot ( \hat \rho^3 \ub) = 0\,, \label{newcomb:rho}
 \\[2mm]
 & \pa_t \hat \Bb^2 - \nabla \times (\ub \times \hat \Bb^2) = 0\,.
\end{align}
\end{subequations}
Here, we used $G \circ \Phi_t^{-1} = \dot\Phi_t \circ \Phi_t^{-1} = \ub$. Therefore, two of the MHD equations came for free. For the pressure, one invokes the adiabatic equation of state of an ideal gas:
\be \label{s:const}
 \dt{} \left( \frac{p}{(\hat \rho^3)^\gamma}  \circ \Phi_t \right) = 0 \,,
\ee
which states that the entropy $s = p/(\hat\rho^3)^\gamma$ is constant along the flow. Using \eqref{newcomb:rho}, this leads to
\be
 \pa_t p + \nabla \cdot (p \ub) + (\gamma - 1)\, p \nabla \cdot \ub = 0\,.
\ee
Note that for the 3-form, equation \eqref{newcomb:rho} is equivalent to
\be 
 \dt{} \left( \hat \rho^3  \circ \Phi_t |\tn{det} D\Phi_t| \right) = 0 \,,
\ee
and thus equation \eqref{s:const} implies
\be \label{dt:p}
 \dt{} \left( \frac{p \circ \Phi_t |\tn{det} D\Phi_t|^\gamma}{(\hat \rho^3 \circ \Phi_t |\tn{det} D\Phi_t|)^\gamma} \right) = \dt{} \left( p \circ \Phi_t |\tn{det} D\Phi_t|^\gamma \right) = 0 \,.
\ee
We can use this to compute the variations $\delta p$. After integrating \eqref{dt:p} from $0$ to $t$ and taking variarions we obtain
\begin{align}
 0 &= \delta \left( p \circ \Phi_t |\tn{det} D\Phi_t|^\gamma \right)
 \\[1mm]
 &= |\tn{det} D\Phi_t|^\gamma \left (\delta p \circ \Phi_t + \delta \Phi_t \cdot \nabla p \circ \Phi_t\right) + p \circ \Phi_t\,\gamma |\tn{det} D\Phi_t|^{\gamma-1} \delta |\tn{det} D\Phi_t|
 \\[1mm]
 &= |\tn{det} D\Phi_t|^\gamma \left (\delta p \circ \Phi_t + \delta \Phi_t \cdot \nabla p \circ \Phi_t + p \circ \Phi_t\,\gamma \, \nabla \cdot \etab \right)\,,
\end{align}
where we used \eqref{del:3} to get to the last line. Evaluating the last line at $\Phi_t^{-1}$ yields
\be
 \delta p = - \etab \cdot \nabla p - \gamma p \nabla \cdot \etab\,.
\ee
The variations corresponding to \eqref{rhoB:free} read, from Theorem \ref{thm:vardel},
\begin{subequations} \label{rhoB:var}
\begin{align}
 &\delta \hat \rho^3 = - \nabla \cdot ( \hat \rho^3 \etab)\,,
 \\[2mm]
 & \delta \hat \Bb^2 = \nabla \times (\etab \times \hat \Bb^2)\,.
\end{align}
\end{subequations}
Taking now the variations of the Newcomb action \eqref{newcombaction} leads to
\be
 0 = \delta S = \int_0^T\int_{\RR^3} \left( \fder{\ell}{\ub} \cdot \delta \ub + \fder{\ell}{\hat \rho^3} \delta\hat \rho^3 + \fder{\ell}{p} \delta p + \fder{\ell}{\hat \Bb^2} \cdot \delta \hat \Bb^2 \right) \tn d \xb\, \tn d t\,,
\ee
Form the Euler-Poincaré theorem \ref{thm:EP} we know how to treat the first term. The remaining terms read
\begin{align}
 \fder{\ell}{\hat \rho^3} \delta\hat \rho^3 &= - \left( \frac{|\ub|^2}{2} - V(\xb)\right) \nabla \cdot ( \hat \rho^3 \etab)\,,
 \\[1mm]
 \fder{\ell}{p} \delta p &= \left( \frac{1}{\gamma - 1} \right)(\etab \cdot \nabla p + \gamma p \nabla \cdot \etab)\,,
 \\[1mm]
 \fder{\ell}{\hat \Bb^2} \cdot \delta \hat \Bb^2 &= - \frac{\hat \Bb^2}{\mu_0} \cdot \nabla \times (\etab \times \hat \Bb^2)\,.
\end{align}
After integrtion by parts we obtain 
\begin{align}
 0 = \delta S &= \int_0^T\int_{\RR^3} \fder{\ell}{\ub} \cdot \delta \ub \, \tn d \xb\, \tn d t
 \\[1mm]
 &+ \int_0^T\int_{\RR^3} \left[ \hat \rho^3 \left( \nabla\ub \cdot \ub - \nabla V \right) + \frac{1}{\gamma - 1} \left(  \nabla p - \gamma \nabla p \right) - \hat \Bb^2 \times \frac{\nabla \times \Bb^2}{\mu_0} \right] \cdot \etab\, \tn d \xb\, \tn d t
\end{align}
Demanding that $\delta S$ be zero for any $\etab$, and using the same cancellation as in \eqref{cancel}, one obtains the following Euler-Poincaré equations with additional terms on the right-hand side:
 $$
 \dt{} (\hat \rho^n \ub) + \nabla \cdot \left( \rho^n \ub \otimes \hat \ub\right) = - \hat \rho^n \nabla V - \nabla p + \frac{\nabla \times \Bb^2}{\mu_0} \times \hat \Bb^2\,.
 $$
This completes the variational derivation of the MHD equations.
