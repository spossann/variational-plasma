\section{Lecture 1: \emph{``Plasma Dynamics and Outline''}}

The outline of the lecture is given in the abstract.

\subsection{The Lorentz force}
Plasma is a gas of charged particles. Charge, like mass, is a fundamental property of matter and comes in two flavors, called positive (`+`) and negative (`-`) charge. While masses always attract each other, charges of opposite (equal) sign attract (repel) each other. In both cases, the interaction is not instantaneous, but mediated by the gravitational field or the electromagnetic field, respectively. Einstein's field equations and Maxwell's equations of electrodynamics provide us with mathematical models for the field dynamics in the presence of mass or charge; in both cases, the wave speed is bound by the speed of light. 

For given electromagnetic fields $\Eb$ and $\Bb$, the force $\Fb$ exerted on a particle with charge $q$ at position $\xb$ with velocity $\vb$ at time $t$ is the {\bf Lorentz force} (in SI units):
\be \label{lorentzforce}
 \Fb(t, \xb) = q \left(\Eb(t, \xb) + \vb \times \Bb(t, \xb)\right)\,,
\ee
The Lorentz force is extremely well verified experimentally and is valid even as approaching relativistic speeds. Neglecting other (much weaker) forces such as Gravitation, Newton's law of motion yields
\be \label{newton}
\begin{aligned}
 \dt{\xb} &= \vb \,, && \xb(0) = \xb_0\,,
 \\[1mm]
 \dt{\vb} &= \frac{q}{m} \left(\Eb(t, \xb) + \vb \times \Bb(t, \xb)\right) && \vb(0) = \vb_0\,,
\end{aligned}
\ee
for the evloution of a charged particle's position $\xb_k(t)$ and velocity $\vb(t)$ that is at $(\xb_0, \vb_0)$ at time $t=0$. However, the computation of the Lorentz force \eqref{lorentzforce} is more complicated as might seem, because the charge itself creates an electromagnetic field as it is moving through space. According to Maxwell's equations (in SI units):
\begin{equation} \label{maxwell}
\begin{aligned}
 - \frac{1}{c^2} \parfra{\Eb}{t} + \nabla \times \Bb &= \mu_0 \jb && \tn{Amp\`ere's law}\,, 
 \\[1mm]
 \parfra{\Bb}{t} + \nabla \times \Eb &= 0 && \tn{Faraday's law}\,,
 \\[2mm]
 \nabla \cdot \Eb &= \frac{\varrho}{\eps_0} && \tn{Gauss' law}\,, 
 \\[2mm]
 \nabla \cdot \Bb &= 0 && \tn{no magnetic monopoles}\,. 
\end{aligned}
\end{equation}
where $c$ denotes the vacuum speed of light, $\eps_0$ and $\mu_0$ stand for the vacuum permittivity and permeability, respectively.\footnote{Setting $\varrho$ and $\jb$ to zero allows for the computation of the vacuum electromagentic radiation, which travels at the speed of light $c$.} In the presence of moving charges indexed by $k$, the charge density $\varrho$ and the current density $\jb$ on the right-hand side are non-zero, thus influencing the fields:
\be
 \varrho(t, \xb) := \sum_k q_k \delta(\xb - \xb_k(t))\,,\qquad \jb(t, \xb) := \sum_k q_k \vb_k(t) \delta(\xb - \xb_k(t))\,,
\ee
where $\delta()$ denotes the Dirac measure.
Therefore, a self-consistent solution of Maxwell's equation together with the particle equations \eqref{newton} gives a complete and exact description of the plasma. In real-world situations, this description is not practicable, because of the enormous number of particles in a macroscopic volume of gas. A big part of plasma Physics is thus concerned with finding reduced approximations for computing $\varrho$ and $\jb$, without resolving each particle trajectory. We will now discuss some of these approximate plasma models.

\subsection{Kinetic models}

Rather than being interested in the exact trajectory of each particle, one could ask for the probability of finding a particle in a certain region of the $(\xb,\vb)$-phase space. The corresponding probability distribution function, denoted by $f_\ts(t, \xb, \vb)$ for species ``s'', is usually just called ``the distribution function'' and satisfies the {\bf Vlasov equation}:
\be \label{vlasov}
 \pder{f_\ts}{t} + \vb \cdot \nabla f_\ts + \frac{q_\ts}{m_\ts} \left( \Eb + \vb \times \Bb \right) \cdot \pder{f_\ts}{\vb} = 0\qquad f_\ts(0) = f_{\ts0}(\xb, \vb)\,.
\ee
This is a transport equation in $(\xb,\vb)$-phase space with initial condition $f_{\ts0}(\xb, \vb)$ and the characteristics given by \eqref{newton}. Denoting the unique solution of \eqref{newton} by $(\Xb(t;\xb_0, \vb_0), \Vb(t;\xb_0, \vb_0))$ we can easily see that $f_\ts$ is constant along these paths:
\be
\begin{aligned}
 \dt{} f_\ts(t, \Xb(t), \Vb(t)) &= \pder{f_\ts}{t} + \dt{\Xb(t)} \cdot \nabla f_\ts + \dt{\Vb(t)} \cdot \pder{f_\ts}{\vb}
 \\[1mm]
 &=\pder{f_\ts}{t} + \Vb(t) \cdot \nabla f_\ts + \frac{q_\ts}{m_\ts} \left[ \Eb(t, \Xb(t)) + \Vb(t) \times \Bb(t, \Xb(t)) \right] \cdot \pder{f_\ts}{\vb} 
 \\[1mm]
 &= 0\,.
 \end{aligned}
\ee
The last equality follows because the Vlasov equation \eqref{vlasov} is assumed to hold for any $(\xb, \vb ) = (\Xb(t), \Vb(t))$ in phase space. A solution to \eqref{vlasov} is thus known once the characteristics have been solved, since
\be
 f_\ts(t, \xb, \vb) = f_\ts(t, \Xb(t;\xb_0, \vb_0), \Vb(t;\xb_0, \vb_0)) = f_{\ts0}(\xb_0, \vb_0)\,.
\ee
In other words, for any point $(\xb, \vb)$ at time $t$, you can find the characteristic that goes through that point (it turn out there is only one if eq. \eqref{newton} is well-posed) and go back to its ``foot'' $(\xb_0,\vb_0)$. The value of the initial condition $f_{\ts0}$ at the foot is the value at $(\xb, \vb)$ at time $t$. This is the basis of important numerical methods like particle-in-cell (PIC) or semi-Lagrangian methods.

Moreover, since the right-hand side of the ODE \eqref{newton} is divergence-free, $\nabla_\xb \cdot \vb = 0$ and $\nabla_\vb \cdot \Fb/m_\ts = 0$, we can write \eqref{vlasov} as
\be \label{vlasov:div}
 \pder{f_\ts}{t} + \nabla_{\xb, \vb} \cdot \left[\begin{pmatrix}
                                      \vb
                                      \\
                                      \Fb/m_\ts 
                                     \end{pmatrix}
 f_\ts \right]= 0\,.
\ee
This expresses the conservation of probability in phase space. Defining the probability of finding a particle of speceis ``s'' in a volume $V$ of phase as 
\be
 P(V) := \int_V f_\ts\,\tn d\xb \tn d\vb\,,
\ee
we can use the divergence theorem to obtain
\be
 \pder{P(V)}{t} = \int_{\partial V} f_\ts \begin{pmatrix}
                                      \vb
                                      \\
                                      \Fb/m_\ts 
                                     \end{pmatrix} \cdot \nb\,
 \tn d\xb \tn d\vb\,,
\ee
where $\nb$ denotes the unit normal to the surface $\pa V$. Hence, the vector field $(\vb, \Fb/m_\ts )$ is the probability flux of $f_\ts$. If the flux through the surface $\pa V$ is zero, probability is conserved. This is the basis of important numercial methods such as finite volume or discontinuous Galerkin methods.

Given multiple species ``s'', the charge and current densities are given by
\be \label{kinetic:rhoj}
 \varrho(t, \xb) = \sum_\ts q_\ts \int f_\ts\, \tn d\vb\,,\qquad \jb(t, \xb) = \sum_\ts q_\ts \int \vb f_\ts\, \tn d\vb\,.
\ee
For kinetic models, the challenge is to solve self-consistently the Maxwell equations \eqref{maxwell} together with the Vlasov equation(s) \eqref{vlasov}, where the coupling is given by \eqref{kinetic:rhoj}. Since the phase space is usually six-dimensional, this is still too expensive for real-world problems, even as super computers reach exa-scale flops. This is because of the multi-scale nature of plasma dynamics, involving space-time scales over many orders of magnitude, which requires a high resolution.


\subsection{Multi-fluid models}

From equation \eqref{kinetic:rhoj} we see that it is sufficient to know the first two velocity moments of $f_\ts$ to compute the source terms in Maxwell's equations. These moments are defined on three-dimensional $\xb$-space, rather than six-dimensional $(\xb, \vb)$-phase space; if we can derive meaningful evolution equations for them, the cost of computing the plasma dynamics could be greatly reduced. Taking the first two velocity moments of the Vlasov equation \eqref{vlasov} yields
\begin{equation} \label{multifluid}
  \begin{aligned}
  &\parfra{n_\ts}{t} + \nabla \cdot (n_\ts\ub_\ts) = 0 \,,  
  \\[2mm]
  &\parfra{}{t}(n_\ts\ub_\ts) + \nabla \cdot (n_\ts\ub_\ts\ub_\ts^\top) + 
\frac{1}{m_\ts}\nabla \cdot \PP_\ts 
  = \frac{q_\ts}{m_\ts}(n_\ts\Eb + n_\ts\ub_\ts\times\Bb) \,,  
 \end{aligned}
\end{equation}
where we defined
\be \label{moms}
 n_\ts = \int f_\ts\,\tn d \vb\,,\qquad n_\ts \ub_\ts = \int \vb f_\ts\,\tn d \vb\,.
\ee
Here, $\PP_\ts$ denotes the pressure tensor,
\be \label{def:P}
 \PP_\ts = m_\ts \int (\vb - \ub_\ts) (\vb - \ub_\ts)^\top f_\ts\,\tn d\vb \,.
\ee
Unfortunately, in order to compute the pressure tensor we need the second velocity moments of $f_\ts$. The evolution of the latter in turn depends on the third velocity moments, and so on. This is known as the {\bf closure problem} of fluid mechanics. Various assumptions can be made to truncate this infinite hierarchy; the most common one is ``collisional closure'', where $f_\ts$ is assumed to be in thermal equilibrium (Gaussian in phase space). This leads to
\be \label{collclosure}
 \tn{collisional closure:}\qquad \PP_\ts = \mathds 1 p_\ts\,,\qquad \dt{} \left(\frac{p_\ts}{n_\ts^\gamma}\right) = 0\,.
\ee
The last relation is called ``polytropic equation of state''; it defines the evolution of the scalar pressure $p_\ts$, where $\gamma$ stands for the heat capacity ration (=5/3 for ideal gases):
\be \label{ppoly}
 \dt{} \left(\frac{p_\ts}{n_\ts^\gamma}\right) = \pder{p_\ts}{t} + \nabla \cdot (p_\ts \ub_\ts ) + (\gamma - 1) p_\ts \nabla \cdot \ub_\ts = 0\,.
\ee
The system \eqref{multifluid} together with the closure \eqref{collclosure} and the Maxwell equations \eqref{maxwell} is a closed system for the plasma dynamics, with
\be \label{multifluid:rhoj}
 \varrho(t, \xb) = \sum_\ts q_\ts n_\ts\,,\qquad \jb(t, \xb) = \sum_\ts q_\ts n_\ts \ub_\ts\,.
\ee
Multi-fluid models are a considerable reduction in terms of numerical cost compared to kinetic models. They are well suited for situations close to thermal equilibrium, or when velocity space effects are negligible. They reduce the burden of dimensionalty, however they do not address the problem of multiple scales. In particular, the multi-fluid system still contains waves with the following frequencies:
\be \label{highfreq}
\begin{aligned}
 \tn{plasma frequency:} \qquad & \Omega_{\tp\ts} = \sqrt{\frac{n_\ts q_\ts^2}{\eps_0 m_\ts}}\,,
 \\[1mm]
 \tn{cyclotron frequency:} \qquad & \Omega_{\tc\ts} = \frac{q_\ts |\Bb|}{m_\ts}\,.
\end{aligned}
\ee
Since the electron mass is by a factor on the order $10^4$ smaller than the ion mass, the above frequencies can be particularly high for electrons. If one is interested only in the ``low frequency'', macroscopic dynamics of the plasma, it is beneficial to filter out these fastest electron frequencies from the model. This leads to so-called single-fluid models.


\subsection{Single-fluid models}

The aim of a single-fluid plasma model is to filter out the highest electron frequencies stated in \eqref{highfreq}. The most prominent single-fluid model is magneto-hydrodynamics (MHD), which we shall derive in the following. For simplicity, let us assume two species, one ion species and electrons. In single-fluid models, one looks at the plasma mass density $N$ and center-of-mass velocity $\Ub$, defined by
\be 
 N = m_\ti n_\ti + m_\te n_\te\,,\qquad N \Ub = m_\ti n_\ti \ub_\ti + m_\te n_\te \ub_\te \,.
\ee
From the two-fluid equations we obtain
\be
\begin{aligned}
 &\pder{N}{t} + \nabla \cdot (N\Ub) = 0\,,
 \\[1mm]
 &\parfra{}{t}(N\Ub) + \sum_\ts m_\ts \nabla \cdot (n_\ts\ub_\ts\ub_\ts^\top) + \nabla p = \varrho \Eb + \jb \times \Bb\,.
 \end{aligned}
\ee
where $p = p_\ti + p_\te$.
There are two main assumptions in MHD:
\begin{itemize}
 \item quasi-neutrality: $\eps_0 \to 0$ (or $\omega/\Omega_{\tp\ts}\to 0$)
 \item mass-less electrons: $m_\te \to 0$
\end{itemize}
In Maxwell's equations \eqref{maxwell}, the assumption of quasi-neutrality leads to 
\be
 \jb = \frac{\nabla \times \Bb}{\mu_0}\,,\qquad 0 = \varrho = q_\ti n_\ti + q_\te n_\te\,.
\ee
Form the mass-less electron assumption we obtain
\be 
 N \to m_\ti n_\ti\,,\qquad \Ub \to \ub_\ti\,, \qquad \sum_\ts m_\ts \nabla \cdot (n_\ts\ub_\ts\ub_\ts^\top) \to  \nabla \cdot (N \Ub\Ub^\top)\,,
\ee
and from the electron momentum equation 
\be
 \nabla p_\te = q_\te n_\te( \Eb + \ub_\te \times \Bb)\,.
\ee
Supposing that the thermal energy is much smaller than the electric energy, $k_\tn{B} T \ll q_\te \phi$, the electron pressure gradient is negligible and we obtain th electric field as
\be
 \Eb = - \ub_\te \times \Bb = - \left(\Ub + \frac{\jb}{q_\te n_\te} \right) \times \Bb \approx - \Ub \times \Bb\,.
\ee
Here, it was assumed that $\jb/(q_\te n_\te) \ll \Ub$. In summary, the MHD equations read
\be \label{mhd}
\tn{MHD} \left\{
\begin{aligned}
 &\pder{N}{t} + \nabla \cdot (N\Ub) = 0\,,
 \\[1mm]
 &\parfra{}{t}(N\Ub) + \nabla \cdot (N \Ub\Ub^\top) + \nabla p = \frac{\nabla \times \Bb}{\mu_0} \times \Bb\,,
 \\[1mm]
 &\pder{p}{t} + \nabla \cdot (p \Ub ) + (\gamma - 1) p\, \nabla \cdot \Ub = 0\,,
 \\[1mm]
 &\pder{\Bb}{t} - \nabla \times (\Ub \times \Bb) = 0\,,
 \\[2mm]
 &\nabla \cdot \Bb = 0\,.
 \end{aligned}
 \right.
\ee
The MHD model is maybe the most prominent model in plasma Physics. Its linear analysis gives an insight into the most relevant plasma waves, namely {\bf Alfvén, slow and fast magnetosonic waves}. Its steady state solution yields {\bf magnetic equilibria}, which play an important role in magnetic confinement fusion, for example.

The MHD system \eqref{mhd} features several conservation properties. For example, the energy
\be
 \cE_\tn{MHD} = \int \frac{N |\Ub|^2}{2}\,\tn d \xb + \frac{1}{\gamma - 1} \int p\,\tn d \xb + \frac{1}{\mu_0} \int \frac{|\Bb|^2}{2}\, \tn d \xb
\ee
is conserved. Moreover, it is evident that $\nabla \cdot \Bb = 0$ holds for all times if it holds at the initial time (take the divergence of the equation for $\Bb$). A third important conserved quantity is the magnetic helicity:
\be
 \cH_\tn{mag} = \int \Ab \cdot (\nabla \times \Ab) \, \tn d \xb\,,
\ee
where we introduced the magnetic vector potential $\Ab$ such that $\Bb = \nabla \times \Ab$ (see Example \ref{ex:maxwell}). Indeed, assuming that boundary integrals vanish,
\be
 \dt{} \cH_\tn{mag} = 2 \int \pa_t \Ab \cdot (\nabla \times \Ab) \, \tn d \xb = 2 \int (\Ub \times \Bb) \cdot \Bb \, \tn d \xb = 0\,.
\ee
The magentic helicity is a measure for the turn and twist and of the magnetic field lines. Its constance implies a constant topology of the magentic field line stucture. This makes reconnection of field lines impossible in the ideal MHD model above (as in solar eruptions). To account for such effects, some dissipation in the form of resistivity has to be added to the model.
