\section{Lecture 2: \emph{``Hamilton's Principle''}} \label{sec:hamsprinc}

There are two main points of view for characterizing motion: 
\begin{enumerate}
 \item Newtonian: local increment of path
 \item Variational: global ``optimization'' of path ({\bf Hamilton's principle} of stationary action).
\end{enumerate}
The two approaches lead to the same dynamics, i.e. an extremum of the \emph{action functional} satisfies Newton's equations of motion. The variational formulation of the dynamics has however several advantages:
\begin{itemize}
 \item It is coordinate independent
 \item Each conserved quantity corresponds to a symmetry of the action functional under a group of transformations ({\bf Noether's theorem})
 \item Coupled models can be derived by adding up actions
 \item Variational integrators can be derived (stable numerics)
 \item Possibility of quantization.
\end{itemize}

\begin{definition}
 The set of all possible states of a system is called the {\bf configuration space}.
\end{definition}

If the configuration space is finite dimensional, we speak of \emph{dynamical systems} (ODEs). If it is infinite dimensional, we speak of {\emph{field theories} (PDEs).


\subsection{Calculus of variations}

We start by formulating a general \emph{action functional} and define what we mean by its variations.
Let $\Omega \subset \RR^p$ be an open subset of $p$-dimensional Euclidean space. We denote by $\xb \in \Omega$ the \emph{independent variables} of the system (e.g. time, spatial coordinates, etc.). Moreover, let $\ub: \Omega \to \RR^q,\,\xb \mapsto\ub(\xb)$ stand for the \emph{dependent variables}, i.e. the $q$ unknowns to be dertermined. The precise space in which we search $\ub$ is problem-dependent, or can be deduced from boundary conditions. Usually we assume smooth $\ub \in C^\infty(\Omega,\RR^q)$, if not stated otherwise. We write 
\be
 \pa_i \ub \equiv \pder{\ub}{x_i}\,,\qquad \nabla := \begin{bmatrix}
                                                      \pa_1 \\
                                                      \vdots \\
                                                      \pa_p
                                                     \end{bmatrix}\,, \qquad \ub^{(n)} \equiv \pder{^n\ub}{x_1 \ldots \pa x_n} \,,\qquad \ub^{(0)} = \ub\,,
\ee
for the derivatives of $\ub$. The generic form of an \emph{action functional} reads
\be \label{action}
 S(\ub) = \int_\Omega L(\xb, \ub^{(n)}(\xb) )\, \tn d \xb\,.
\ee
Here, the \emph{Lagrangian} $L$ is assumed to be a smooth function of $\xb$, $\ub$ and its derivatives.

\begin{example}
 The length of a smooth curve in the plane $\RR^2$, given as the graph of a function $u(x)$, with $u(a) = b$ and $u(c) = d$, is given by
 \be
  S(u) = \int_a^c \sqrt{1 + (\pa_x u)^2} \,\tn d x\,.
 \ee
 The straight line between the points $(a,b)$ and $(c,d)$ in $\RR^2$ is the minimum of the above functional.
\end{example}

\begin{definition} \label{def:varf}
 Let $\ub$ be some function $\Omega \to \RR^q$. Suppose $\tilde \ub(\xb,\eps)$ is a smooth function $\Omega \times \RR \to \RR^q$ such that $\tilde \ub(\xb,0) = \ub(\xb)$. The {\bf variation of a function} $\ub$ is defined as
 \be
  \delta \ub(\xb) := \dvar \tilde \ub(\xb,\eps)\,.
 \ee
\end{definition}

In the simplest case, the function $\tilde \ub$ is given by $\tilde \ub = \ub + \eps\,\delta\ub$.

\begin{definition}
 Let $\ub$ and $\tilde \ub$ as in Definition \ref{def:varf}. The {\bf variation of a functional} $S$ as given in \eqref{action} at $\ub$ is defined as
 \be
  \delta S(\ub) := \dvar S(\tilde \ub(\xb, \eps))\,.
 \ee
\end{definition}

\begin{example} \label{ex:var1}
 Let us consider the functional
 \be
 S(\ub) = \int_\Omega L(\xb, \ub(\xb))\,\tn d \xb\,, 
 \ee
 with some smooth Lagrangian $L$.
 Its variation at $\ub$ is
 \be
 \begin{aligned}
  \delta S(\ub) = \dvar S(\tilde \ub(\xb, \eps)) &= \int_\Omega \dvar L(\xb, \tilde \ub(\xb,\eps))\,\tn d \xb
  \\[2mm]
  &= \sum_{i=1}^q\int_\Omega \left[ \pder{L}{u_i}(\xb, \tilde \ub) \deps{\tilde u_i(\xb,\eps)} \right]_{\eps=0} \tn d \xb
  \\[2mm]
  &= \int_\Omega \left[ \pder{L}{\ub}(\xb, \tilde \ub) \cdot \deps{\tilde \ub(\xb,\eps)} \right]_{\eps=0} \tn d \xb
  \\[2mm]
  &= \int_\Omega \pder{L}{\ub}(\xb, \ub) \cdot \delta \ub\, \tn d \xb
  \\[2mm]
 \end{aligned}
 \ee
\end{example}

\begin{definition}
 The {\bf variational derivative} $\delta S / \delta \ub$ of a functional $S$ as given in \eqref{action} is defined via the relation
 \be
 \delta S(\ub) = \int_\Omega \fder{S}{\ub} \cdot \delta \ub\,\tn d \xb\,.
 \ee
\end{definition}

The variational derivative $\fder{S}{\ub}$ can be viewed as a linear functional acting on the space of variations $\delta\ub$. Hence, it lives in the dual of the space of variations. In Example \ref{ex:var1} for instance, we found
\be
 \fder{S}{\ub} = \pder{L}{\ub}(\xb, \ub)\,.
\ee

\begin{prop}
 If $\ub_0$ is an extremal of $S(\ub)$, then 
 \be
  \fder{S}{\ub}(\ub_0(\xb)) = 0\qquad \xb \in \Omega\,.
 \ee
 \begin{proof}
 \be
 \begin{aligned}
  &\tn{$\ub_0$ is an extremal of $S$ } 
  \\[1mm]
  \implies &f(\eps) := S(\tilde\ub_0(\xb, \eps)) \tn{ has an extremal at $\eps=0$}
  \\[1mm]
  \implies &\dvar f(\eps) = 0
  \\[1mm]
  \implies & \delta S(\ub_0) = 0 \quad \forall\, \delta \ub
  \\[1mm]
  \implies & \fder{S}{\ub}(\ub_0(\xb)) = 0\qquad \xb \in \Omega\,.
 \end{aligned}
 \ee
 \end{proof}
\end{prop}

\begin{example}
 Consider the Laplace equation $\Delta u(\xb) = 0$ for $\xb \in \Omega$ (scalar field $u$ or $q = 1$) with homogeneous Dirichlet boundary conditions, i.e. $u=0$ on $\pa\Omega$. The solution $u$ is an extremal of the functional 
 \be
  S(u) = \frac 12 \int_\Omega |\nabla u|^2\,\tn d\xb\,.
 \ee
 The variation of $S$ reads
 \be
 \begin{aligned}
  \delta S(u) &= \dvar S(\tilde u(\xb,\eps)) 
  \\
  &= \frac 12 \int_\Omega \dvar |\nabla \tilde u(\xb,\eps)|^2\,\tn d\xb
  \\
  &= \int_\Omega \dvar L(\nabla \tilde u)\,\tn d\xb
  \\
  &= \sum_{i=1}^p \int_\Omega \pder{L}{(\pa_i u)} \dvar \pa_i \tilde u(\xb,\eps) \,\tn d\xb
  \\
  &=  \int_\Omega \pder{L}{(\nabla u)} \cdot \dvar \nabla \tilde u(\xb,\eps) \,\tn d\xb
  \\
  &= \int_\Omega \pder{L}{(\nabla u)} \cdot \nabla \dvar  \tilde u(\xb,\eps) \,\tn d\xb
  \\
  &= - \int_\Omega \nabla \cdot \pder{L}{(\nabla u)} \delta u \,\tn d\xb + \int_{\pa\Omega}  \pder{L}{(\nabla u)} \cdot \mathbf n\, \delta u \,\tn d\xb\,.
 \end{aligned}
 \ee
If we demand that the variations $\delta u$ satisfy the same boundary conditions as $u$, that is $\delta u=0$ on $\pa\Omega$, the boundary term vanishes. Moreover, setting $\delta S(u) = 0$ and noting that $\delta u$ is arbitrary implies
\be
 - \nabla \cdot \pder{L}{(\nabla u)} = - \nabla \cdot \nabla u(\xb) = 0 \qquad \xb \in \Omega\,.
\ee
\end{example}



\subsection{Hamilton's principle} \label{sec:hamprinc}

From now we shall be concerned with dynamical systems, meaning that one of the independent variables is the time $t \in \RR$, such that $\ub(t,\cdot)$ describes the state of the system at time $t$.
Hamilton's principle of stationary action states that the correct motion $\ub$ of a dynamical system satisfies
\be \label{ds=0}
\delta S(\ub) = 0\,,
\ee
where $S$ is an action of the form \eqref{action}. The principle is equivalent to a set of dynamical equations called the \emph{Euler-Lagrange equations}. The subject is best approached in a finite-dimensional configuration space. In what follows, let $\Omega=(0, T) \subset \RR$ and $t \in \Omega$ denote the only independent variable $(p=1)$. Moreover, let $\ub: \Omega \to \RR^q,\, t\mapsto \ub(t)$ stand for the dependent variables, which can be viewed as trajectories in Euclidean space $\RR^q$, which is the configuration space of this problem. Let us consider a generic action functional
\be \label{S1}
 S(\ub) = \int_0^T L(t, \ub(t),\dot\ub(t))\,\tn d t\,,
\ee
where the Lagrangian $L(t, \ub, \dot \ub)$ is a smooth function of $2q+1$ arguments, evaluated at $t$, $\ub(t)$ and the first derivative $\dot \ub(t)$ under the action integral. The variation of the action reads
\be
\begin{aligned}
 \delta S(\ub) &= \dvar S(\tilde \ub(t, \eps))
 \\[1mm]
 &= \int_0^T \dvar L(t, \tilde \ub(t,\eps),\dot{\tilde\ub}(t,\eps))\,\tn d t
 \\[1mm]
 &= \int_0^T \left[\pder{L}{\ub} \cdot \deps \tilde \ub(t,\eps) + \pder{L}{\dot\ub} \cdot \deps{} \dt{} \tilde{\ub}(t,\eps)  \right]_{\eps=0}\,\tn d t
 \\[1mm]
 &= \int_0^T \left[\pder{L}{\ub} \cdot \delta\ub - \dt{}\pder{L}{\dot\ub} \cdot \delta \ub  \right]\,\tn d t + \pder{L}{\dot\ub} \cdot \delta \ub\bigg|_0^T
\end{aligned}
\ee
Assuming that the variations vanish at the endpoints, $\delta \ub(0) = \delta\ub(T) = 0$, Hamilton's principle $\delta S(\ub) = 0$ yields
\be \label{eulag1}
 \fder{S}{\ub} = \pder{L}{\ub} - \dt{}\pder{L}{\dot\ub} = 0\,,
\ee
which are the Euler-Lagrange equations corresponding to the action \eqref{S1}. Remark that the derivatives of the Lagrangian in the above expression are evaluated at $(t, \ub(t),\dot\ub(t))$. Therefore, a solution $\ub(t)$ of the Euler-Lagrange ODEs \eqref{eulag1} is an extremal of the action $S(\ub(t))$.

An immediate feature of \eqref{eulag1} is that if $L$ does not depend on a coordinate $u_i$, i.e. $\pa L /\pa u_i = 0$, the corresponding conjugate momentum $\pa L/\pa \dot u_i$ is conserved:
\be
 \dt{}\pder{L}{\dot u_i}(t, \ub(t),\dot\ub(t)) = 0\,.
\ee

\begin{example}
 In classical mechanics, the correct motion of a system can often be obtained from a Lagrangian of the form $L = T - V$, where $T$ stands for the kinetic energy and $V$ denotes the potential energy. As an example, let us consider the {\bf central force problem} in $\RR^2$. The Newtonian equations of motion read
 \be
 \begin{aligned}
  \ddot x(t) &= - \pa_x V\left(\sqrt{x(t)^2 + y(t)^2}\right)
  \\[1mm]
  \ddot y(t) &= - \pa_y V\left(\sqrt{x(t)^2 + y(t)^2}\right)\,,
 \end{aligned}
 \ee
 where $V= V(r)$ is a central force potential depending only on the distance to the origin. It thus makes sense to express this problem in polar coordinates $(r, \theta) \in \RR^+ \times [0,2\pi)$,
 \be
 x = r \cos \theta\,,\qquad y = r \sin\theta\,.
 \ee
 The kinetic energy of a particle with mass $m$ reads
 \be
  K(x(t), y(t)) = \frac m2 (\dot x(t)^2 + \dot y(t)^2) \,.
 \ee
 From 
 \be
 \begin{aligned}
  \dot x &= \dot r \cos \theta - r \dot \theta \sin \theta
  \\[1mm]
  \dot y &= \dot r \sin \theta + r \dot \theta \cos \theta
 \end{aligned}
 \ee
 we obtain
 \be
  \dot x^2 + \dot y^2 = \dot r^2 (\cos^2 \theta + \sin^2\theta) + r^2 \dot\theta^2 (\sin^2\theta + \cos^2\theta) = \dot r^2 + r^2 \dot\theta^2\,.
 \ee
 Hence, the Lagrangian $L = K - V$ in polar coordinates is given by
 \be
  L = \frac m2 (\dot r^2 + r^2 \dot\theta^2) - V(r)\,.
 \ee
 We immediately see that $L$ does not depend on $\theta$, so that the Euler-Lagrange equations \eqref{eulag1} imply 
 \be
  \dt{} \pder{L}{\dot\theta}(r(t), \theta(t), \dot r(t), \dot \theta(t)) = \dt{} m\, r^2(t) \dot \theta(t) = 0\,.
 \ee
 This is the conservation of angular momentum $\cA_0 = m r^2(0) \dot\theta(0)$, which is given to us automatically by the Euler-Lagrange equations. For the radial coordinate we obtain
 \be
  m \ddot r = mr\dot\theta^2 - V'(r) = \frac{\cA_0^2}{mr^3} - V'(r)\,.
 \ee
\end{example}

Euler-Lagrange equations can also be derived for infinite-dimensional configuration spaces. For the application of plasma physics, the most relevant configuration spaces are $C^\infty(\Omega,\RR)$ and  $C^\infty(\Omega,\RR^3)$ (and products thereof), i.e. spaces of smooth (vector-valued) functions on an open subset $\Omega \subset\RR^3$. The most common differential operators are grad, curl and divergence. For dynamical vector fields $\ub(t) \in C^\infty(\Omega,\RR^3)\,\forall\,t \in \RR$, the generic action functional is
\be
 S(\ub) = \int_0^T\int_\Omega L(t,\ub,\dot\ub,\nabla \times \ub, \nabla \cdot \ub)\,\tn d \xb\,\tn d t\,.
\ee
We shall always assume that boundary conditions on $\ub$ are such that boundary integrals vanish. It is then straightforward to show that the corresponding Euler-Lagrange equations read
\be \label{eulag:vec}
\fder{S}{\ub} = \pder{L}{\ub} - \pder{}{t} \pder{L}{\dot\ub} + \nabla \times \pder{L}{(\nabla \times \ub)} - \nabla \, \pder{L}{(\nabla \cdot \ub)} = 0\,.
\ee
For dynamical scalar fields $u(t) \in C^\infty(\Omega,\RR)\,\forall\,t \in \RR$, the generic action functional is
\be
 S(u) = \int_0^T\int_\Omega L(t,u,\dot u,\nabla u)\,\tn d \xb\,\tn d t\,,
\ee
with the corresponding Euler-Lagrange equations 
\be \label{eulag:scal}
 \fder{S}{u} = \pder{\cL}{u} - \pder{}{t} \pder{\cL}{\dot u} - \nabla \cdot \pder{\cL}{(\nabla u)} = 0\,.
\ee

\begin{example} \label{ex:maxwell}
 {\bf Maxwell's equations} \eqref{maxwell} can be derived from a variational principle. For this, one introduces potentials $\phi$ and $\Ab$ such that the electromagnetic fields are given by
 \be
  \Eb = -\nabla \phi - \pa_t \Ab\,,\qquad \Bb = \nabla \times \Ab\,.
 \ee
 By this "ansatz", two of Maxwell's equations are satisfied automatically, namely Faraday's law and no magnetic monopoles:
 \be
 \implies\qquad  \nabla \times \Eb = -\pa_t\Bb\,,\qquad \nabla \cdot \Bb = 0\,. 
 \ee
 However, the choice of $\phi$ and $\Ab$ is not unique, because for any scalar field $g$, the transformations
 \be \label{gauge}
  \phi \mapsto \phi - \pa_t g\,,\qquad \Ab \mapsto \Ab + \nabla g\,,
 \ee
 yield the same electromagnetic fields. The transformation \eqref{gauge} is called a \emph{gauge transformation}. Some of the more popular electromagnetic gauges are
\begin{enumerate}
 \item the Coulomb gauge $\nabla \cdot \Ab = 0$,
 \item the Weyl gauge $\phi = 0$,
 \item the Lorenz gauge $\nabla \Ab + \pa_t \phi/c^2 = 0$.
\end{enumerate}
 Ampères' law and Guass' law can be derived from a variational principle for $\phi$ and $\Ab$. The action functional is
 \be
 S(\phi, \Ab) = \int_0^T\int_\Omega \left[ \frac{\eps_0}{2} |\nabla \phi + \pa_t \Ab|^2 - \frac{1}{2\mu_0} |\nabla \times \Ab|^2 - \phi \varrho + \jb \cdot \Ab \right]\,\tn d \xb\,\tn d t\,,
 \ee
 where $\varrho$ and $\jb$ are given functions.
 Using \eqref{eulag:scal}, it is easy to calculate the functional derivative of $S$ with respect to $\phi$:
 \be
 \fder{S}{\phi} = - \varrho - \eps_0\, \nabla \cdot (\nabla \phi + \pa_t \Ab) = - \varrho + \eps_0\, \nabla \cdot \Eb = 0\,. 
 \ee
 This is Gauss' law. Similarly, using \eqref{eulag:vec} we obtain the functional derivative of $S$ with respect to $\Ab$:
 \begin{align*}
  \pder{S}{\Ab} &= \jb - \eps_0\,\pa_t (\nabla \phi + \pa_t \Ab) - \frac{1}{\mu_0} \nabla \times (\nabla \times \Ab)
  \\[1mm]
  &= \jb + \eps_0\,\pa_t \Eb - \frac{1}{\mu_0} \nabla \times \Bb = 0\,.
 \end{align*}
 Multiplying by $\mu_0$ and using $\eps_0 \mu_0 = 1/c^2$ yields Ampère's law.
\end{example}



