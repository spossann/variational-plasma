\section{Lecture 6: \emph{``Transport theorems for $p$-forms''}} \label{sec:transp}

Some of Physics most prominent equations are expressions of \emph{local conservation laws}. The word "local" is critical here as it means that conservation laws hold at any point on the manifold. This is in contrast to "global" conservation laws, which speak about quantities related to the whole system, such as its total energy or mass. Local conservation laws usually lead to partial differential equations (PDEs), involving space-time derivatives. A prime example of a local conservation law is the continuity equation of mass or charge. Kinetic equations like the Vlasov equation in plasma physics express conservation of probability density in phase space. The induction equation of magneto-hydrodynamics (MHD) is the statement of conservation of magnetic flux along the fluid flow (frozen-in condition). There are many such examples in Physics and several different approaches of how to understand them mathematically. Flow maps (section \ref{sec:flow}) combined with differential forms (section \ref{sec:forms}) provide a useful set of techniques for such an endeavour. 

For an $n$-dimensional manifold, there are $n+1$ differential $p$-forms, whose dimensions are given by $\binom{n}{p}$ for $p=0,1,\ldots,n$. This means there are always two scalar-valued $p$-forms, namely the $0$-forms (point values) and the $n$-forms (volume integrals). All other $p$-forms have vector-valued coefficient functions, usually of different dimension, and describe integration over $p$-dimensional hyper-surfaces. For the scalar $0$-forms and $n$-forms, as well as for the ubiquitous 1-forms, we shall derive local conservation laws for arbitrary dimension $n$. For 2-forms, we focus on the three-dimensional case for simplicity in the derivation of the local conservation law. Three dimensions are usually enough for the description of classical, non-relativistic Physics we focus on in this manuscript. The results will be presented as a series of transport theorems for the different $p$-forms. 

\begin{theorem}
    \textbf{(Transport of 0-forms.)}
     Let $\Phi_t:\qb_0 \in \RR^n\mapsto \qb \in \RR^n$ be the diffeomorphism group associated to the autonomous IVP \eqref{ivp:0} with direction field $G:\RR^n\to \RR^n$. For any differentiable function $\hat f^0:\RR \times \RR^n \to \RR$ (component function of a 0-form), the following statements are equivalent:
     \begin{enumerate}
         \item $\hat f^0$ is constant along $\Phi_t$,
          $$
            \hat f^0(t, \Phi_t(\qb_0)) = \hat f^0(0, \qb_0)\,.
          $$
          \item The time derivative of $\hat f^0$ along the path $t\mapsto \Phi_t(\qb_0)$ is zero,
          $$
            \dt{} \hat f^0(t, \Phi_t(\qb_0)) = 0\,.
          $$
          \item $\hat f^0$ satisfies the PDE
             $$
                \pa_t \hat f^0 + G\cdot \nabla \hat f^0 = 0\qquad \tn{on} \ \Omega\,.
             $$ 
          \item The $0$-form $f^0 = \hat f^0$ satisfies
           $$
            \pa_t f^0 + \cL_Gf^0 = 0\,,
           $$
           where $\cL_G$ denotes the Lie derivative.
     \end{enumerate}
     \begin{proof}
        \phantom{a}\\
         1. $\to$ 2.: differentiate both sides w.r.t time $t$ (total derivative).\\
         2. $\to$ 1.: integrate from $0$ to $t$ and apply the fundamental theorem of Calculus.\\
         2. $\leftrightarrow$ 3.: use that the $\Phi_t$ satisifes the IVP \eqref{ivp:0}:
         \begin{align*}
             &\dt{} \hat f^0(t, \Phi_t(\qb_0)) = 0
             \\
             \Leftrightarrow \ \ &\pa_t \hat f^0 + \dot{\Phi_t}\cdot \nabla \hat f^0 = 0
             \\[2mm]
             \Leftrightarrow \ \ &\pa_t \hat f^0 + G\cdot \nabla \hat f^0 = 0 \,.
         \end{align*}
         Finally set $\qb = \Phi_t(\qb_0)$ and use that this is one-to-one.
     \end{proof}
\end{theorem}

\begin{theorem} \label{transp:vol}
    \textbf{(Transport of n-forms in $\RR^n$.)}
     Let $\Phi_t:\qb_0 \in \RR^n\mapsto \qb \in \RR^n$ be the diffeomorphism group associated to the autonomous IVP \eqref{ivp:0} with direction field $G:\RR^n\to \RR^n$. For any differentiable function $\hat f^n:\RR \times \RR^n \to \RR$ (component function of a $n$-form), the following statements are equivalent:
     \begin{enumerate}
         \item Any volume integral over $\hat f^n$ is constant when advected with $\Phi_t$,
          $$
            \int_{\Phi_t(\mathcal V_0)}\hat f^n(t, \qb)\,\tn d\qb = \int_{\mathcal V_0}\hat f^n(0, \qb_0)\,\tn d\qb_0\,,\qquad \forall 
 \ \mathcal V_0 \subseteq \Omega\,.
          $$
          \item The following total time derivative is zero,
          $$
            \dt{} \left[\hat f^n(t, \Phi_t(\qb_0)) \,|\tn{det} \,D\Phi_t| \right] = 0\,.
          $$
          \item $\hat f^n$ satisfies the PDE
             $$
                \pa_t \hat f^n + \nabla \cdot (G\hat f^n) = 0\qquad \tn{on} \ \Omega\,.
             $$
          \item The $n$-form $f^n = \hat f^n\, \tn d^n\qb$ satisfies
           $$
            \pa_t f^n + \cL_Gf^n = 0\,,
           $$
           where $\cL_G$ denotes the Lie derivative.
     \end{enumerate}
     \begin{proof}
        \phantom{a}\\
         1. $\to$ 2.: in the left integral, consider the change of variables $\qb = \Phi_t(\qb_0)$ to obtain
         $$
            \int_{\mathcal V_0}\hat f^n(t, \Phi_t(\qb_0)) \,|\tn{det}\,D\Phi_t|\,\tn d\qb_0 = \int_{\mathcal V_0}\hat f^n(0, \qb_0)\,\tn d\qb_0\,.
          $$
         Then differentiate both sides w.r.t time $t$ (total derivative).\\
         2. $\to$ 1.: integrate from $0$ to $t$ and apply the fundamental theorem of Calculus. Then integrate the result over any volume $\mathcal V_0$ and insert the change of variables $\qb = \Phi_t(\qb_0)$.\\
         2. $\leftrightarrow$ 3.: apply the product rule, use that the $\Phi_t$ satisifes the IVP \eqref{ivp:0} and also use \eqref{dt:det} from Theorem \ref{thm:flow} (denoting $J_t = \tn{det}\,D\Phi_t $):
         \begin{align*}
             &\dt{} \left[\hat f^n(t, \Phi_t(\qb_0)) \,|J_t| \right]  = 0
             \\
             \Leftrightarrow \ \ & |J_t| \dt{}\hat f^n(t, \Phi_t(\qb_0)) + \hat f^n(t, \Phi_t(\qb_0)) \dt{}|J_t| = 0
             \\
             \Leftrightarrow \ \ &|J_t| \left(\pa_t \hat f^n + \dot{\Phi_t}\cdot \nabla \hat f^n \right) + \hat f^n\, |J_t| \nabla \cdot G = 0
             \\[2mm]
             \Leftrightarrow \ \ &\pa_t \hat f^n +  \nabla \cdot ( G \hat f^n) = 0 \,.
         \end{align*}
         Finally set $\qb = \Phi_t(\qb_0)$ and use that this is one-to-one.
     \end{proof}
\end{theorem}

\begin{theorem}
    \textbf{(Transport of 1-forms.)}
     Let $\Phi_t:\qb_0 \in \RR^n\mapsto \qb \in \RR^n$ be the diffeomorphism group associated to the autonomous IVP \eqref{ivp:0} with direction field $G:\RR^n\to \RR^n$. For any differentiable function $\hat \Eb^1:\RR \times \RR^n \to \RR^n$  (component function of a 1-form), the following statements are equivalent:
     \begin{enumerate}
         \item Any line integral (along a line $\cL_0$) over $\hat \Eb^1$ is constant when advected with $\Phi_t$,
          $$
            \int_{\Phi_t(\mathcal L_0)}\hat \Eb^1(t, \qb)\cdot\tn d \qb = \int_{\mathcal L_0}\hat \Eb^1(0, \qb_0)\cdot\tn d\qb_0\,,\qquad \forall 
 \ \mathcal L_0 \subset \Omega\,.
          $$
          \item The following total time derivative is zero,
          $$
            \dt{} \left[D\Phi_t^\top\,\hat \Eb^1(t, \Phi_t(\qb_0)) \right] = 0\,.
          $$
          \item $\hat \Eb^1$ satisfies the PDE
             $$
                \pa_t \hat \Eb^1 + G\cdot \nabla \hat\Eb^1 + \nabla G \cdot \hat\Eb^1 = 0\qquad \tn{on} \ \Omega\,.
             $$
          \item The 1-form $E^1 = \hat \Eb^1 \cdot \tn d \qb$ satisfies
           $$
            \pa_t E^1 + \cL_GE^1 = 0\,,
           $$
           where $\cL_G$ denotes the Lie derivative.
     \end{enumerate}
     \begin{proof}
        \phantom{a}\\
         1. $\to$ 2.: in the left integral, consider the change of variables $\qb = \Phi_t(\qb_0)$ and use the pullback of 1-forms \eqref{pull:1} to obtain
         $$
            \int_{\mathcal L_0} D\Phi_t^\top\,\hat\Eb^1(t, \Phi_t(\qb_0)) \cdot\tn d\qb_0 = \int_{\mathcal L_0}\hat \Eb^1(0, \qb_0)\cdot\tn d\qb_0\,.
          $$
         Then differentiate both sides w.r.t time $t$ (total derivative).\\
         2. $\to$ 1.: integrate from $0$ to $t$ and apply the fundamental theorem of Calculus. Then integrate the result over any line $\mathcal L_0$ and insert the change of variables $\qb = \Phi_t(\qb_0)$.\\
         2. $\leftrightarrow$ 3.: apply the product rule, use that the $\Phi_t$ satisifes the IVP \eqref{ivp:0} and also use \eqref{dt:jac} from Theorem \ref{thm:flow}:
         \begin{align*}
             &\dt{} \left[D\Phi_t^\top\,\hat \Eb^1(t, \Phi_t(\qb_0)) \right] = 0
             \\
             \Leftrightarrow \ \ & D\Phi_t^\top \dt{}\hat \Eb^1(t, \Phi_t(\qb_0)) + \left(\dt{}D\Phi_t^\top \right) \Eb^1(t, \Phi_t(\qb_0)) = 0
             \\
             \Leftrightarrow \ \ &D\Phi_t^\top \left(\pa_t \hat \Eb^1 + \dot{\Phi_t}\cdot \nabla \hat \Eb^1 \right) + D\Phi_t^\top \nabla G\cdot\hat\Eb^1 = 0
             \\[2mm]
             \Leftrightarrow \ \ &\pa_t \hat \Eb^1 + G\cdot \nabla \hat\Eb^1 + \nabla G \cdot \hat\Eb^1 = 0 \,.
         \end{align*}
         Finally set $\qb = \Phi_t(\qb_0)$ and use that this is one-to-one.
     \end{proof}
\end{theorem}

\begin{theorem}
    \textbf{(Transport of 2-forms in $\RR^3$.)}
     Let $\Phi_t:\qb_0 \in \RR^3\mapsto \qb \in \RR^3$ be the diffeomorphism group associated to the autonomous IVP \eqref{ivp:0} with direction field $G:\RR^3\to \RR^3$. For any differentiable function $\hat \Eb^2:\RR \times \RR^3 \to \RR^3$  (component function of a 2-form), the following statements are equivalent:
     \begin{enumerate}
         \item Any surface integral (over a surface $\cS_0$) over $\hat \Eb^2$ is constant when advected with $\Phi_t$,
          $$
            \int_{\Phi_t(\mathcal S_0)}\hat \Eb^2(t, \qb)\cdot\tn d \boldsymbol \sigma = \int_{\mathcal S_0}\hat \Eb^2(0, \qb_0)\cdot\tn d \boldsymbol\sigma_0\,,\qquad \forall 
 \ \mathcal S_0 \subset \Omega\,.
          $$
          \item The following total time derivative is zero,
          $$
            \dt{} \left[J_t \,D\Phi_t^{-1}\,\hat \Eb^2(t, \Phi_t(\qb_0)) \right] = 0\,.
          $$
          \item $\hat \Eb^2$ satisfies the PDE
             $$
                \pa_t \hat \Eb^2 + G\cdot \nabla \hat\Eb^2 - \hat \Eb^2 \cdot \nabla G + \hat\Eb^2\,\nabla \cdot G = 0\qquad \tn{on} \ \Omega\,.
             $$
             In case that $\nabla \cdot \hat \Eb^2 = 0$, we have
             $$
                \pa_t \hat \Eb^2 - \nabla \times ( G \times \hat \Eb^2 ) = 0\qquad \tn{on} \ \Omega\,.
             $$
          \item The 2-form $E^2 = \hat \Eb^2 \cdot \tn d \boldsymbol \sigma$ satisfies
           $$
            \pa_t E^2 + \cL_GE^2 = 0\,,
           $$
           where $\cL_G$ denotes the Lie derivative.
     \end{enumerate}
     \begin{proof}
        \phantom{a}\\
         1. $\to$ 2.: in the left integral, consider the change of variables $\qb = \Phi_t(\qb_0)$ and use the pullback of 2-forms \eqref{pull:2} to obtain
         $$
            \int_{\mathcal S_0} J_t\,D\Phi_t^{-1}\,\hat\Eb^2(t, \Phi_t(\qb_0)) \cdot\tn d\boldsymbol \sigma_0 = \int_{\mathcal S_0}\hat \Eb^2(0, \qb_0)\cdot\tn d \boldsymbol \sigma_0\,.
          $$
         Then differentiate both sides w.r.t time $t$ (total derivative).\\
         2. $\to$ 1.: integrate from $0$ to $t$ and apply the fundamental theorem of Calculus. Then integrate the result over any surface $\mathcal S_0$ and insert the change of variables $\qb = \Phi_t(\qb_0)$.\\
         2. $\leftrightarrow$ 3.: apply the product rule, use that the $\Phi_t$ satisfies the IVP \eqref{ivp:0} and also use \eqref{dt:jac} and \eqref{dt:jacinv} from Theorem \ref{thm:flow}:
         \begin{align*}
             &\dt{} \left[J_t \,D\Phi_t^{-1}\,\hat \Eb^2(t, \Phi_t(\qb_0)) \right] = 0
             \\
             \Leftrightarrow \ \ & J_t\,D\Phi_t^{-1} \dt{}\hat \Eb^2(t, \Phi_t(\qb_0)) + J_t\left(\dt{}D\Phi_t^{-1} \right) \Eb^2(t, \Phi_t(\qb_0)) + \left(\dt{} J_t \right) D\Phi_t^{-1}\,\hat \Eb^2(t, \Phi_t(\qb_0) = 0
             \\
             \Leftrightarrow \ \ &J_t\,D\Phi_t^{-1} \left(\pa_t \hat \Eb^2 + \dot \Phi_t \cdot \nabla \hat\Eb^2\right)  - J_t\,D\Phi_t^{-1} \nabla G^\top \hat\Eb^2(t, \Phi_t(\qb_0)) +  J_t\, D\Phi_t^{-1}\,\nabla \cdot G\,\hat \Eb^2(t, \Phi_t(\qb_0) = 0
             \\[2mm]
             \Leftrightarrow \ \ &\pa_t \hat \Eb^2 + G\cdot \nabla \hat\Eb^2 - \hat \Eb^2 \cdot \nabla G + \nabla \cdot G \, \hat\Eb^2 = 0 \,.
         \end{align*}
         Finally set $\qb = \Phi_t(\qb_0)$ and use that this is one-to-one.
          In case that $\nabla \cdot \hat \Eb^2 = 0$, we can use the vector idetitiy
          $$
          \nabla \times (\ab \times \bb) = \ab\,\nabla \cdot \bb - \bb\,\nabla \cdot \ab + \bb \cdot \nabla \ab - \ab \cdot \nabla \bb\,,
          $$
          to obtain
         \begin{align*}
          \pa_t \hat \Eb^2 &= - G\cdot \nabla \hat\Eb^2 + \hat \Eb^2 \cdot \nabla G - \hat\Eb^2\nabla \cdot G + G \nabla \cdot \hat \Eb^2
          \\[1mm]
          &= \nabla \times (G \times \hat \Eb^2)\,.
         \end{align*}
     \end{proof}
\end{theorem}
